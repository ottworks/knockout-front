// @ts-check
const express = require('express');
const fs = require('fs');
const helmet = require('helmet');
const compression = require('compression');
const http = require('http');
const https = require('https');
const path = require('path');

const fsp = fs.promises;

const metaInjector = require('./server/metaInjector');

const app = express();

app.use(helmet());
app.use(compression());

// serve static assets with cache control enabled
app.use(
  '/static',
  express.static(path.join(__dirname, '/dist/client/static'), {
    redirect: false,
    fallthrough: true,
    setHeaders: (res) => {
      // revalidate static files (like images, etc) every 12 hours
      res.setHeader('Cache-Control', 'public, max-age: 43200');
      res.setHeader('Service-Worker-Allowed', '/');
    },
  }),
  (req, res) => {
    res.status(404).send('File not found');
  }
);

// handle every other route with index.html, which will contain
// a script tag to your application's JavaScript file(s).
app.get('*', async (req, res) => {
  try {
    const metaTags = await metaInjector(req);
    if (metaTags) {
      res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate');
      res.setHeader('Pragma', 'no-cache');
      res.setHeader('Expires', '0');
      res.setHeader('Surrogate-Control', 'no-store');
      res.setHeader('Service-Worker-Allowed', '/');

      const file = await fsp.readFile(path.join(__dirname, '/dist/client/index.html'), 'utf8');

      const editedHtml = file.replace(
        `<meta name="description" content="A welcoming gaming and lifestyle community!"><meta name="viewport" content="width=device-width,initial-scale=1"><meta name="theme-color" content="#16161a"><meta name="twitter:dnt" content="on"><link rel="manifest" href="/static/manifest.json"><meta property="og:title" content="Knockout Forums"/><meta property="og:description" content="A welcoming gaming and lifestyle community!"/><meta property="og:type" content="website"/><meta property="og:url" content="https://knockout.chat/"/><meta property="og:image" content="https://knockout.chat/static/logo.png"/>`,
        `${metaTags}`
      );

      res.set('Content-Type', 'text/html');
      res.send(Buffer.from(editedHtml));
      return res;
    }
  } catch (error) {
    console.log('Error when sending a metatagged HTML:');
    console.log(error);
  }
  // always check if there is a newer version of the index page
  // to avoid serving pages that reference old scripts after updates
  res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, proxy-revalidate');
  res.setHeader('Pragma', 'no-cache');
  res.setHeader('Expires', '0');
  res.setHeader('Surrogate-Control', 'no-store');
  res.setHeader('Service-Worker-Allowed', '/');
  res.sendFile(path.join(__dirname, '/dist/client/index.html'));
});

const PORT_HTTP = 8080;
const PORT_HTTPS = 443;

console.log('Starting servers...');
if (process.env.NODE_ENV === 'development') {
  const httpServer = http.createServer(app);
  httpServer.listen(PORT_HTTP, () => {
    console.log(`🚀 HTTP Server running on port ${PORT_HTTP}`);
  });
} else if (process.env.NODE_ENV === 'qa') {
  const privateKey = fs.readFileSync(
    '/etc/letsencrypt/live/forums.stylepunch.club/privkey.pem',
    'utf8'
  );
  const certificate = fs.readFileSync(
    '/etc/letsencrypt/live/forums.stylepunch.club/cert.pem',
    'utf8'
  );
  const ca = fs.readFileSync('/etc/letsencrypt/live/forums.stylepunch.club/fullchain.pem', 'utf8');

  const credentials = {
    key: privateKey,
    cert: certificate,
    ca,
  };

  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(PORT_HTTPS, () => {
    console.log(`🚀 HTTPS Server running on port ${PORT_HTTPS}`);
  });

  const httpsRedirect = express().use((req, res) => {
    res.redirect(301, `https://${req.host}${req.url}`);
  });
  httpsRedirect.listen(PORT_HTTP, () => {
    console.log(`👮 Redirecting requests on port ${PORT_HTTP} to HTTPS`);
  });
} else if (process.env.NODE_ENV === 'production') {
  const httpServer = http.createServer(app);
  httpServer.listen(8001, () => {
    console.log(`🚀 HTTP Server running on port 8001`);
  });
} else {
  console.error('Error: Please set NODE_ENV to "development", "qa" or "production" ');
  process.exit(1);
}

module.exports = app;
