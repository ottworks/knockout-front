const fs = require('fs');
const path = require('path');
const dotenv = require('dotenv');

// Load .env files
const envFiles = [`.env.example`, `.env.${process.env.NODE_ENV}`, `.env`];
const envConfig = {};

for (const file of envFiles) {
  if (fs.existsSync(file)) {
    const envPath = path.resolve(process.cwd(), file);

    Object.assign(envConfig, dotenv.config({ path: envPath }).parsed);
  }
}

// Transform SNAKE_CASE keys into camelCase
const appConfig = {};

for (const [key, value] of Object.entries(envConfig)) {
  const newKey = key.toLowerCase().replace(/_([a-z])/g, (_, c) => c.toUpperCase());

  if (value === 'True' || value === 'False') { // dotenv doesnt coerce booleans properly. boo!
    appConfig[newKey] = value === 'True'; // this should be a boolean
  } else {
    appConfig[newKey] = value;
  }
}

process.appConfig = appConfig;
module.exports = appConfig;
