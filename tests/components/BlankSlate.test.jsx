import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import BlankSlate from '../../src/componentsNew/BlankSlate';

describe('BlankSlate component', () => {
  it('displays the resource name which could not be found', () => {
    const { getByText } = render(<BlankSlate resourceNamePlural="Resources" />);
    expect(getByText('Sorry, no Resources were found.')).toBeInTheDocument();
  });
});
