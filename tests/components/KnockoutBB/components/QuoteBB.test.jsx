import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import QuoteBB from '../../../../src/componentsNew/KnockoutBB/components/QuoteBB';
import { customRender, screen } from '../../../custom_renderer';

describe('Post Quote', () => {
  it('displays the quoted text', () => {
    customRender(
      <QuoteBB username="Shmeegus" threadPage="1" threadId="1" postId="1">
        <p>This is a quote</p>
      </QuoteBB>
    );
    expect(screen.queryByText('This is a quote')).toBeInTheDocument();
  });

  it('displays the name of the quoted poster', () => {
    customRender(
      <QuoteBB username="Shmeegus" threadPage="1" threadId="1" postId="1">
        <p>This is a quote</p>
      </QuoteBB>
    );
    expect(screen.queryByText('Shmeegus posted:')).toBeInTheDocument();
  });
});
