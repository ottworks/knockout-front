import React from 'react';
import { waitFor } from '@testing-library/react';
import { customRender, fireEvent } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import RepliesMenu from '../../../../src/componentsNew/Header/components/RepliesMenu';

describe('RepliesMenu component', () => {
  const initialState = {
    mentions: {
      mentions: [],
    },
  };

  const initialStateWithMention = {
    mentions: {
      mentions: [
        {
          mentionId: 123,
          threadId: 456,
          postId: 789,
          threadTitle: 'Thread',
          mentionedBy: { username: 'Bob', usergroup: 1, isBanned: false },
          content: '["A mention!"]',
        },
      ],
    },
  };

  it('renders nothing when no replies are present', () => {
    const { queryByTitle } = customRender(<RepliesMenu />, { initialState });
    expect(queryByTitle('Replies')).toBeNull();
  });

  it('displays the menu when the icon is clicked', () => {
    const { getByTitle, queryByText } = customRender(<RepliesMenu />, {
      initialState: initialStateWithMention,
    });
    fireEvent.click(getByTitle('Replies'));
    expect(queryByText('Replies')).not.toBeNull();
  });

  it('displays unread replies', () => {
    const { getByTitle, queryByText, queryByTitle } = customRender(<RepliesMenu />, {
      initialState: initialStateWithMention,
    });
    fireEvent.click(getByTitle('Replies'));
    expect(queryByText('Thread.')).not.toBeNull();
    expect(queryByText('Bob')).not.toBeNull();
    expect(queryByTitle('Replies').querySelector('.link-notification')).toHaveTextContent('1');
  });

  it('marks unread replies as read', async () => {
    const state = {
      mentions: {
        mentions: [
          {
            mentionId: 123,
            threadId: 456,
            postId: 789,
            threadTitle: 'Thread',
            mentionedBy: { username: 'Bob', usergroup: 1, isBanned: false },
            content: '["A mention!"]',
          },
          {
            mentionId: 2,
            threadId: 4,
            postId: 7,
            threadTitle: 'Another Thread',
            mentionedBy: { username: 'Joe', usergroup: 1, isBanned: false },
            content: '["A mention!"]',
          },
        ],
      },
    };
    const { getByTitle, getByText, getAllByTitle, queryByText, queryByTitle } = customRender(
      <RepliesMenu />,
      {
        initialState: state,
      }
    );
    fireEvent.click(getByTitle('Replies'));
    expect(queryByText('Thread.')).not.toBeNull();
    expect(queryByText('Bob')).not.toBeNull();
    expect(queryByText('Another Thread.')).not.toBeNull();
    expect(queryByText('Joe')).not.toBeNull();
    expect(queryByTitle('Replies').querySelector('.link-notification')).toHaveTextContent('2');

    fireEvent.click(getAllByTitle('Dismiss')[0]);
    expect(queryByText('Thread.')).toBeNull();
    expect(queryByText('Bob')).toBeNull();
    await waitFor(() => getByText('1'));
  });

  it('displays the old mention format', () => {
    const state = {
      mentions: {
        mentions: [
          {
            mentionId: 123,
            threadId: 456,
            postId: 789,
            content: '["A mention!"]',
          },
        ],
      },
    };
    const { getByTitle, queryByText, queryByTitle } = customRender(<RepliesMenu />, {
      initialState: state,
    });
    fireEvent.click(getByTitle('Replies'));
    expect(queryByText('A mention!')).not.toBeNull();
    expect(queryByTitle('Replies').querySelector('.link-notification')).toHaveTextContent('1');
  });
});
