/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import dayjs from 'dayjs';
import BanItem from '../../../../src/views/UserProfile/components/BanItem';
import { USER_GROUPS } from '../../../../src/utils/userGroups';
import { customRender } from '../../../custom_renderer';

describe('BanItem component', () => {
  let defaultProps = {};
  const prevExpiration = dayjs().subtract(3, 'day').toISOString();
  const futureExpiration = dayjs().add(1, 'month').toISOString();
  const currentDate = dayjs().subtract(1, 'hour').toISOString();

  beforeEach(() => {
    defaultProps = {
      banReason: 'Stuff',
      createdAt: currentDate,
      expiresAt: futureExpiration,
      duration: '1 month',
      banStillValid: true,
      bannedBy: {
        username: 'Bob',
        avatarUrl: '',
        backgroundUrl: '',
        createdAt: '',
        id: 1,
        usergroup: USER_GROUPS.MODERATOR,
      },
      isMod: false,
      invalidateBan: jest.fn(),
      thread: { title: 'Cool things', id: 3 },
      post: { page: 1, id: 2 },
    };
  });

  it('displays ban details', () => {
    const { queryByText } = customRender(<BanItem {...defaultProps} />);
    expect(queryByText('an hour ago')).not.toBeNull();
    expect(queryByText('Cool things')).not.toBeNull();
  });

  it('displays ban expiration', () => {
    const { queryByText } = customRender(<BanItem {...defaultProps} />);
    expect(queryByText('Expires in a month')).not.toBeNull();
  });

  it('displays past ban expiration', () => {
    defaultProps.expiresAt = prevExpiration;
    defaultProps.banStillValid = false;
    const { queryByText } = customRender(<BanItem {...defaultProps} />);
    expect(queryByText('Expired 3 days ago')).not.toBeNull();
  });

  it('properly hides thread details', () => {
    defaultProps.post = undefined;
    const { queryByText } = customRender(<BanItem {...defaultProps} />);
    expect(queryByText('an hour ago')).not.toBeNull();
    expect(queryByText('Cool things')).toBeNull();
  });

  it('hides mod controls', () => {
    const { queryByText } = customRender(<BanItem {...defaultProps} />);
    expect(queryByText('Unban')).toBeNull();
  });

  it('shows unban button for mods', () => {
    defaultProps.isMod = true;
    const { queryByText } = customRender(<BanItem {...defaultProps} />);
    expect(queryByText('Unban')).not.toBeNull();
  });

  it('hides unban button for expired bans', () => {
    defaultProps.isMod = true;
    defaultProps.banStillValid = false;
    const { queryByText } = customRender(<BanItem {...defaultProps} />);
    expect(queryByText('Unban')).toBeNull();
  });
});
