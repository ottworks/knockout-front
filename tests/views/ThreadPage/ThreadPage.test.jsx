/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { customRender } from '../../custom_renderer';
import store from '../../../src/state/configureStore';
import ThreadPage from '../../../src/views/ThreadPage/ThreadPage';

describe('ThreadPage component', () => {
  let defaultProps = {};

  beforeEach(() => {
    localStorage.clear();
    defaultProps = {
      posts: [],
      thread: {
        title: 'New thread',
        user: { id: 0 },
        iconId: 0,
        id: 0,
        locked: false,
        deleted: false,
        subforum: {
          name: '',
        },
        subforumId: 0,
        subscriptionLastSeen: new Date(),
        readThreadLastSeen: new Date(),
        totalPosts: 0,
      },
      params: {
        id: 0,
      },
      showingMoveModal: false,
      moveModalOptions: [],
      currentPage: 0,
      currentUserId: 0,
      togglePinned: jest.fn(),
      toggleLocked: jest.fn(),
      toggleDeleted: jest.fn(),
      showMoveModal: jest.fn(),
      deleteAlert: jest.fn(),
      createAlert: jest.fn(),
      submitThreadMove: jest.fn(),
      hideModal: jest.fn(),
      refreshPosts: jest.fn(),
      getEditor: jest.fn(),
      goToPost: jest.fn(),
      linkedPostId: 0,
    };
  });

  it('displays the thread move modal', () => {
    const { queryByText } = customRender(<ThreadPage {...defaultProps} showingMoveModal />);
    expect(queryByText('Move thread')).not.toBeNull();
  });

  describe('as a non-logged in user', () => {
    it('hides the post editor', () => {
      const { queryByTitle } = customRender(<ThreadPage {...defaultProps} />);
      expect(queryByTitle('Shortcut: Ctrl+Enter')).toBeNull();
    });
  });

  describe('as a logged in user', () => {
    const loggedInState = {
      user: {
        loggedIn: true,
        username: 'TestUser',
      },
    };

    const userLocalStorageDetails = {
      id: 123,
      username: 'TestUser',
      usergroup: 1,
      avatarUrl: 'avatar.png',
    };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
    });

    it('displays the post editor', () => {
      const { queryByText } = customRender(<ThreadPage {...defaultProps} />, {
        initialState: loggedInState,
      });
      expect(queryByText('Formatting Help')).not.toBeNull();
    });

    it('hides the post editor if the thread is locked', () => {
      defaultProps.thread.locked = true;
      const { queryByText } = customRender(<ThreadPage {...defaultProps} />, {
        initialState: loggedInState,
      });
      expect(queryByText('Formatting Help')).toBeNull();
    });

    it('hides the post editor if the thread is deleted', () => {
      defaultProps.thread.deleted = true;
      const { queryByText } = customRender(<ThreadPage {...defaultProps} />, {
        initialState: loggedInState,
      });
      expect(queryByText('Formatting Help')).toBeNull();
    });

    describe('as a moderator', () => {
      const moderatorState = {
        user: {
          id: 3,
          username: 'Mod',
          loggedIn: true,
          usergroup: 3,
          avatarUrl: 'avatar.png',
        },
      };

      const modLocalStorageDetails = moderatorState.user;

      beforeEach(() => {
        localStorage.__STORE__.currentUser = JSON.stringify(modLocalStorageDetails);

        jest.spyOn(store, 'getState').mockImplementation(() => moderatorState);
      });

      it('shows the post editor if the thread is locked', () => {
        defaultProps.thread.locked = true;
        const { queryByText } = customRender(<ThreadPage {...defaultProps} />, {
          initialState: moderatorState,
        });
        expect(queryByText('Formatting Help')).not.toBeNull();
      });
    });
  });
});
