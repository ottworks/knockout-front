import * as UserUtils from '../../src/utils/user';
import store from '../../src/state/configureStore';
import * as userService from '../../src/services/user';
import * as notifications from '../../src/utils/notification';

describe('checkLoginStatus function', () => {
  const defaultPath = '/';
  const defaultUpdateHeader = jest.fn();
  const history = [];

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('componentDidMount', () => {
    describe('when the user is not logged in', () => {
      beforeEach(() => {
        jest.spyOn(store, 'getState').mockReturnValue({ user: { loggedIn: false } });
      });

      it('returns false, as the user is not logged in', () => {
        expect(
          UserUtils.checkLoginStatus(defaultPath, defaultUpdateHeader, history)
        ).resolves.toBeFalsy();
      });
    });

    describe('when the user is logged in', () => {
      beforeEach(() => {
        jest.spyOn(store, 'getState').mockReturnValue({ user: { loggedIn: true } });
      });

      it('returns false if the the current path is /usersetup, as the user is still in setup', () => {
        expect(
          UserUtils.checkLoginStatus('/usersetup', defaultUpdateHeader, history)
        ).resolves.toBeFalsy();
      });

      describe('and the user data syncs successfully', () => {
        let updateHeader;

        beforeEach(() => {
          jest.spyOn(userService, 'syncData').mockReturnValue({ username: 'TestUser' });
          updateHeader = jest.fn();
        });

        it('updates the header with the user', async () => {
          await UserUtils.checkLoginStatus(defaultPath, updateHeader, history);
          expect(updateHeader).toHaveBeenCalledWith({ username: 'TestUser' });
        });

        describe('and the user is banned', () => {
          let mockHistory;

          beforeEach(() => {
            jest.useFakeTimers();
            jest.spyOn(userService, 'syncData').mockReturnValue({ isBanned: true });
            mockHistory = {
              push: jest.fn(),
            };
          });

          it('forces the user to logout', async () => {
            await UserUtils.checkLoginStatus(defaultPath, defaultUpdateHeader, mockHistory);
            expect(setTimeout).toHaveBeenCalledTimes(1);
            jest.runAllTimers();
            expect(mockHistory.push).toHaveBeenCalledWith('/logout');
          });
        });
      });

      describe('and there is an error returned from the API', () => {
        beforeEach(() => {
          jest
            .spyOn(userService, 'syncData')
            .mockReturnValue({ error: 'Oh no there was an error!' });
        });

        it('pushs a notification out', async () => {
          const pushNotificationFn = jest.spyOn(notifications, 'pushNotification');
          await UserUtils.checkLoginStatus(defaultPath, defaultUpdateHeader, history);
          expect(pushNotificationFn).toHaveBeenCalled();
        });
      });

      describe('and the user data does not sync successfully', () => {
        beforeEach(() => {
          jest.spyOn(userService, 'syncData').mockReturnValue(undefined);
        });

        it('pushs a notification out', async () => {
          const pushNotificationFn = jest.spyOn(notifications, 'pushNotification');
          await UserUtils.checkLoginStatus(defaultPath, defaultUpdateHeader, history);
          expect(pushNotificationFn).toHaveBeenCalled();
        });
      });
    });
  });
});

describe('isLoggedIn', () => {
  it('returns the current loggedIn status of the user in the store', () => {
    jest.spyOn(store, 'getState').mockReturnValue({ user: { loggedIn: true } });
    expect(UserUtils.isLoggedIn()).toBeTruthy();
    jest.clearAllMocks();
  });
});
