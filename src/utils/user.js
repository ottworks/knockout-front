import { syncData } from '../services/user';
import store from '../state/configureStore';
import { pushNotification } from './notification';

export const isLoggedIn = () => store.getState().user.loggedIn;

export async function checkLoginStatus(currentPath, updateHeader, history) {
  if (isLoggedIn() && currentPath !== '/usersetup') {
    const user = await syncData();

    // if we get no response, notify the user that
    // the request failed:
    if (!user || user.error) {
      pushNotification({
        message:
          '⚠️ Could not sync your user data - things might not work properly. Please try again later or try logging out and back in.',
        type: 'error',
      });
    } else {
      updateHeader(user);

      // if the user is banned, log them out.
      // ban local storage info and notification
      // is done in the user.js service
      if (user.isBanned) {
        setTimeout(() => {
          history.push('/logout');
        }, 3500);
      }
      return true;
    }
    return false;
  }
  return false;
}
