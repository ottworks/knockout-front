import { getEventQuotes } from './eventDates';

const cachedVersion = '3';

export const loadQuotesFromStorage = () => {
  const quotes = JSON.parse(localStorage.getItem(`quotes${cachedVersion}`));

  const eventQuotes = getEventQuotes();

  if (eventQuotes) {
    return eventQuotes;
  }

  if (quotes) {
    return quotes;
  }
  return null;
};

export const saveQuotesToStorage = data => {
  const quotes = JSON.stringify(data);

  localStorage.setItem(`quotes${cachedVersion}`, quotes);
};

export const clearQuotesFromStorage = () => {
  localStorage.removeItem(`quotes${cachedVersion}`);
};
