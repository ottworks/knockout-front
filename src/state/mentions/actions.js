export const MENTION_UPDATE = 'MENTION_UPDATE';
export const MENTION_REMOVE = 'MENTION_REMOVE';

export function updateMentions(value) {
  return {
    type: MENTION_UPDATE,
    value,
  };
}

export function removeMentions(value) {
  return {
    type: MENTION_REMOVE,
    value,
  };
}
