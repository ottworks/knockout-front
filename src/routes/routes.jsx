import React from 'react';
import loadable from 'loadable-components';

import Loading from '../componentsNew/Loading';

const LoadingComponent = () => <Loading />;

// refactored routes
export const HomePage = loadable(() => import('../views/HomePage'), { LoadingComponent });
export const SubforumPage = loadable(() => import('../views/SubforumPage'), {
  LoadingComponent,
});
export const ThreadPage = loadable(() => import('../views/ThreadPage'), { LoadingComponent });
export const ThreadCreationPage = loadable(() => import('../views/ThreadCreationPage'), {
  LoadingComponent,
});
export const EventsPage = loadable(() => import('../views/EventsPage'), { LoadingComponent });
export const UserSettingsPage = loadable(() => import('../views/UserSettingsPage'), {
  LoadingComponent,
});
export const AlertsList = loadable(() => import('../views/AlertsList'), { LoadingComponent });
export const Rules = loadable(() => import('../views/RulesPage'), { LoadingComponent });
export const UserSetup = loadable(() => import('../views/UserSetupPage'), { LoadingComponent });
export const UserProfile = loadable(() => import('../views/UserProfile/index'), {
  LoadingComponent,
});
export const LoginPage = loadable(() => import('../views/LoginPage'), { LoadingComponent });
export const KnockoutBBSyntaxPage = loadable(() => import('../views/KnockoutBBSyntaxPage'), {
  LoadingComponent,
});
export const Moderate = loadable(() => import('../views/Moderation/'), { LoadingComponent });

// search
export const SearchPage = loadable(() => import('../views/SearchPage'), {
  LoadingComponent,
});

export const Logout = loadable(() => import('../views/Logout'), {
  LoadingComponent,
});
