import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import ratingList from '../../utils/ratingList.json';
import Tooltip from '../Tooltip';

import {
  ThemeBackgroundDarker,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeFontSizeSmall,
  ThemePostLineHeight,
} from '../../utils/ThemeNew';
import { pushNotification } from '../../utils/notification';
import submitRating from '../../services/ratings';
import allRatings from '../../utils/ratingGroups';
import RatingItem from './components/RatingItem';
import componentOpenAnim from '../../utils/componentOpenAnim';

const RatingBar = ({ postId, xrayEnabled, ratings, ratingDisabled, refreshPost }) => {
  const animationTransition = 400;

  const [userListOpen, setUserListOpen] = useState(false);
  const [xrayOpenAnim, setXrayOpenAnim] = useState(false);
  const [voterOpen, setVoterOpen] = useState(false);
  const [voted, setVoted] = useState(false);
  const loggedIn = useSelector((state) => state.user.loggedIn);

  const toggleUserList = () => {
    componentOpenAnim(userListOpen, setUserListOpen, setXrayOpenAnim, animationTransition);
  };

  const ratePost = async (rating) => {
    if (ratingDisabled) {
      return;
    }

    if (!loggedIn) {
      pushNotification({ message: 'You must be signed in to rate a post.' });
      return;
    }

    setVoted(true);
    setVoterOpen(false);

    try {
      await submitRating({ postId, rating });
      refreshPost(postId);
    } catch (error) {
      pushNotification({ message: 'Unable to rate post.', type: 'error' });
    }

    setTimeout(() => {
      setVoted(false);
    }, 2000);
  };

  return (
    <>
      <StyledRatingBar>
        <div className={`user-ratings${voterOpen ? ' voter-open ' : ''}`}>
          <div className="rating-bar">
            {ratings.map((rating) => (
              <RatingItem
                key={rating.rating}
                rating={rating.rating}
                count={rating.count}
                onClick={() => ratePost(rating.rating)}
              />
            ))}
            {xrayEnabled && ratings.length > 0 && (
              <div className="rating-xray rating-bar-item" key="xray">
                <button
                  className="xray-expander"
                  title="Rating XRay"
                  onClick={toggleUserList}
                  type="button"
                >
                  {userListOpen ? <i className="fas fa-eye-slash" /> : <i className="fas fa-eye" />}
                </button>
              </div>
            )}
          </div>
        </div>
        {!ratingDisabled && loggedIn && (
          <div className={`rating-list ${voted ? ' voted ' : ''}${voterOpen ? ' open ' : ''}`}>
            <div className="rating-bar">
              {allRatings.map((ratingName) => (
                <div key={ratingList[ratingName].name} className="rating-bar-item rating-item">
                  <Tooltip text={ratingList[ratingName].name} top>
                    <button
                      type="button"
                      onClick={() => ratePost(ratingName)}
                      aria-label={`Rate ${ratingList[ratingName].name}`}
                      title={`Rate ${ratingList[ratingName].name}`}
                    >
                      <img src={ratingList[ratingName].url} alt={ratingList[ratingName].name} />
                    </button>
                  </Tooltip>
                </div>
              ))}
            </div>
            <div className="rated-message">Rated!</div>
            <button
              className="mobile-rate-button"
              onClick={() => setVoterOpen((value) => !value)}
              type="button"
              title={voterOpen ? 'Close Rating Menu' : 'Open Rating Menu'}
            >
              {voterOpen ? 'Close' : 'Rate'}
            </button>
          </div>
        )}
      </StyledRatingBar>
      {ratings && xrayEnabled && userListOpen && (
        <XrayBar open={xrayOpenAnim} transition={animationTransition} title="Rating x-ray list">
          <ul className="xray-rating-list xray-scrollbar">
            {ratings.map((rating) => (
              <li key={ratingList[rating.rating].name}>
                <img src={ratingList[rating.rating].url} alt={ratingList[rating.rating].name} />
                <ul className="xray-user-list xray-scrollbar">
                  {rating.users.map((user) => (
                    <li key={`${postId}_${user}`}>{user}</li>
                  ))}
                </ul>
              </li>
            ))}
          </ul>
        </XrayBar>
      )}
    </>
  );
};

RatingBar.propTypes = {
  postId: PropTypes.number.isRequired,
  xrayEnabled: PropTypes.bool.isRequired,
  ratings: PropTypes.arrayOf(
    PropTypes.shape({
      rating: PropTypes.string.isRequired,
      count: PropTypes.number.isRequired,
      users: PropTypes.arrayOf(PropTypes.string).isRequired,
    })
  ),
  ratingDisabled: PropTypes.bool.isRequired,
  refreshPost: PropTypes.func.isRequired,
};
RatingBar.defaultProps = {
  ratings: [],
};

export const StyledRatingBar = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  min-height: 50px;
  .user-ratings,
  .rating-list {
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    min-height: 40px;
    display: flex;
    align-items: center;
    .rating-bar {
      padding: 0;
      margin: 0;
      display: flex;
      align-items: center;
      .rating-bar-item {
        display: inline-block;
        button {
          background-color: transparent;
          border: none;
          cursor: pointer;
          transition: opacity 100ms ease-in-out;
          &:focus {
            outline: none;
          }
        }
        &.rating-item {
          button {
            color: ${ThemeTextColor};
          }
          img {
            height: 23px;
            width: 23px;
            vertical-align: middle;
          }
          span.count {
            font-family: 'Open Sans', sans-serif;
            display: block;
            margin-top: calc(${ThemeHorizontalPadding} / 2);
            font-size: ${ThemeFontSizeSmall};
            opacity: 0.7;
            vertical-align: bottom;
          }
        }
      }
    }
  }
  .rating-list {
    .rating-bar {
      flex-wrap: wrap;
    }
  }
  .user-ratings {
    .rating-bar {
      .rating-xray {
        vertical-align: middle;
        button {
          color: ${ThemeTextColor};
          opacity: 0.3;
          &:hover {
            opacity: 0.7;
          }
        }
      }
    }
    @media (max-width: 700px) {
      &.voter-open {
        pointer-events: none;
      }
    }
  }
  .rating-list {
    align-self: flex-end;
    opacity: 0.3;
    transition: opacity 100ms ease-in-out;
    &:hover {
      opacity: 1;
    }
    .rating-item {
      &:hover {
        img {
          filter: drop-shadow(0px 0px 3px #ffcc00);
        }
      }
    }
    .rated-message {
      display: none;
      text-align: right;
    }
    button.mobile-rate-button {
      display: none;
      color: ${ThemeTextColor};
      background: transparent;
      border: none;
      opacity: 0.7;
      cursor: pointer;
      transition: opacity 100ms ease-in-out;
      &:focus {
        outline: none;
      }
      &:hover {
        opacity: 1;
      }
    }
    &.voted {
      opacity: 0.3;
      .rating-bar {
        display: none;
      }
      button.mobile-rate-button {
        display: none;
      }
      .rated-message {
        display: inline-block;
      }
    }
    @media (max-width: 700px) {
      display: flex;
      opacity: 1;
      .rating-bar {
        display: none;
        margin-left: auto;
        margin-right: 0;
        .rating-bar-item {
          margin: 5px 0px;
        }
      }
      button.mobile-rate-button {
        display: inline-block;
        margin-left: auto;
      }
      &.open {
        background: ${ThemeBackgroundDarker};
        position: absolute;
        right: 0;
        left: 0;
        .rating-bar {
          display: inline-block;
        }
        button.mobile-rate-button {
          margin-left: 0;
        }
      }
    }
  }
`;

export const XrayBar = styled.div`
  border-top: 1px solid rgba(0, 0, 0, 0.3);
  width: 100%;
  overflow-y: hidden;
  max-height: ${(props) => (props.open ? '110px' : '0px')};
  transition: ${(props) => props.transition / 1000}s;
  background-color: ${ThemeBackgroundDarker};
  background-color: transparent;
  margin-bottom: ${ThemeVerticalPadding};
  ul {
    list-style: none;
  }
  ul.xray-rating-list {
    padding: 8px 0;
    overflow-x: auto;
    overflow-y: hidden;
    white-space: nowrap;
    max-height: 110px;
    scrollbar-width: thin;
    display: flex;

    > li {
      display: inline-block;
      vertical-align: top;
      position: relative;
      max-height: 84px;
      > img {
        height: 23px;
        width: 23px;
        left: 10px;
        position: absolute;
      }
    }
  }
  ul.xray-user-list {
    padding-left: 25px;
    padding-right: 10px;
    margin-left: 10px;
    max-width: 140px;
    overflow-y: auto;
    overflow-x: hidden;
    display: inline-block;
    scrollbar-width: thin;
    max-height: inherit;
    height: 100%;
    line-height: calc(${ThemePostLineHeight}% * 0.7);
    li {
      font-size: ${ThemeFontSizeSmall};
    }
  }
  .xray-scrollbar {
    &::-webkit-scrollbar-track {
      background-color: rgba(black, 0.2);
    }

    &::-webkit-scrollbar {
      width: 10px;
      height: 10px;
      background-color: rgba(0, 0, 0, 0.1);
    }

    &::-webkit-scrollbar-thumb {
      background-color: rgba(0, 0, 0, 0.3);
    }
  }
`;

export default RatingBar;
