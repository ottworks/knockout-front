import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '../../Tooltip';
import ratingList from '../../../utils/ratingList.json';

const RatingItem = ({ rating, count, onClick }) => {
  return (
    <div className="rating-bar-item rating-item">
      <div className="rating-icon">
        <Tooltip text={ratingList[rating].name} top>
          <button
            onClick={onClick}
            className="rating-button"
            type="button"
            aria-label={`Rate ${ratingList[rating].name}`}
          >
            <img src={ratingList[rating].url} alt={ratingList[rating].name} />
            <span className="count">{`${count}`}</span>
          </button>
        </Tooltip>
      </div>
    </div>
  );
};

RatingItem.propTypes = {
  rating: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};
export default RatingItem;
