import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/**
 * A styled image block component.
 *
 * @type {Component}
 */
const StyledImage = styled('img')`
  display: inline-block;
  max-width: 100%;
  max-height: 100vh;

  ${(props) => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
  ${(props) => (props.isThumbnail || props.isLink) && `border: 1px dotted gray;`}

  ${(props) =>
    props.isThumbnail &&
    ` max-width: 300px;
      max-height: 300px;
    `}
`;

/*
 * A function to determine whether a URL has an image extension.
 *
 * @param {String} url
 * @return {Boolean}
 */
export const isImage = (url) => {
  const imageExtensions = ['png', 'jpg', 'jpeg', 'gif', 'webp'];
  const rgx = /(?:\.([^.]+))?$/;

  const extension = rgx.exec(url);

  return !!imageExtensions.includes(extension[1]);
};

const ImageBB = ({ href, thumbnail, link }) => {
  if (link || thumbnail) {
    return link ? (
      <a href={href} target="_blank" rel="ugc noopener">
        <StyledImage src={href} isThumbnail={thumbnail} isLink={link} />
      </a>
    ) : (
      <a href={href} target="_blank" rel="ugc noopener">
        <StyledImage src={href} isThumbnail={thumbnail} />
      </a>
    );
  }

  return <StyledImage src={href} />;
};

ImageBB.propTypes = {
  href: PropTypes.string.isRequired,
  thumbnail: PropTypes.bool.isRequired,
  link: PropTypes.bool.isRequired,
};

export default ImageBB;
