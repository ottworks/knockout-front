/* eslint-disable no-useless-escape */
/* eslint-disable prefer-destructuring */
import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';
import { getYoutubeId } from './YoutubeBB';

/**
 * A styled image block component.
 *
 * @type {Component}
 */
const StyledVimeoWrapper = styled.div`
  display: block;
  margin: 15px 0;

  position: relative;
  width: 100%;
  max-width: 960px;

  &:before {
    content: '';
    width: 1px;
    margin-left: -1px;
    float: left;
    height: 0;
    padding-top: calc(540 / 960 * 100%);
  }
  &:after {
    content: '';
    display: table;
    clear: both;
  }

  ${(props) => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
`;
const StyledVimeo = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;
const StyledVimeoPlaceholder = styled.div`
  display: block;
  margin: 15px 0;

  position: relative;
  width: 100%;
  max-width: 960px;

  img {
    width: 100%;
    height: 100%;
    max-height: 540px;
  }

  &:before {
    content: '';
    width: 1px;
    margin-left: -1px;
    float: left;
    height: 0;
    padding-top: calc(540 / 960 * 100%);
  }
  &:after {
    display: table;
    clear: both;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    content: '\f27d';
    font-family: 'Font Awesome 5 Brands', sans-serif;
    font-size: 100px;
  }

  &:hover {
    img {
      filter: brightness(1.1);
    }
    &:after {
      opacity: 0.2;
    }
  }
`;

export const getVimeoId = (url) => {
  const regex = /(http|https)?:\/\/(www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|)(\d+)(?:|\/\?)/;

  try {
    const result = regex.exec(url);

    if (!result[4]) {
      return null;
    }

    return result[4];
  } catch (error) {
    return null;
  }
};

const VimeoBB = ({ content, href }) => {
  const [visible, setVisible] = useState(false);

  const onChange = (isVisible) => isVisible && setVisible(isVisible);

  try {
    const vimeoUrl = href || `${content}`;
    const vimeoId = getVimeoId(vimeoUrl);

    return visible ? (
      <StyledVimeoWrapper>
        <StyledVimeo
          title={vimeoId}
          type="text/html"
          width="100%"
          height="auto"
          src={`https://player.vimeo.com/video/${vimeoId}?title=0&byline=0&portrait=0`}
          frameBorder="0"
          allowFullScreen="allowfullscreen"
        />
      </StyledVimeoWrapper>
    ) : (
      <VisibilitySensor partialVisibility minTopValue={150} intervalDelay="250" onChange={onChange}>
        <StyledVimeoPlaceholder onClick={() => onChange(true)} title="Play Vimeo video">
          <img
            src={`https://img.youtube.com/vi/${getYoutubeId(
              'https://www.youtube.com/watch?v=WLDeuIAFOyo'
            )}/hqdefault.jpg`}
            alt="Thumbnail for Vimeo video"
          />
        </StyledVimeoPlaceholder>
      </VisibilitySensor>
    );
  } catch (error) {
    return '[Bad Vimeo embed.]';
  }
};

VimeoBB.propTypes = {
  src: PropTypes.string.isRequired,
};

export default VimeoBB;
