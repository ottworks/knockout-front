import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export const isReddit = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'www.reddit.com') throw new Error();
    return true;
  } catch (error) {
    return null;
  }
};

export const useAsyncScript = (src) => {
  useEffect(() => {
    const script = document.createElement('script');
    script.src = src;
    script.async = true;
    document.body.appendChild(script);

    return () => document.body.removeChild(script);
  }, [src]);
};

const RedditWrapper = styled.div`
  .embedly-card .embedly-card-hug {
    background: white;
  }
`;

const RedditBB = ({ href, children }) => {
  useAsyncScript('//embed.redditmedia.com/widgets/platform.js');
  const url = href || children.join('');

  return (
    <RedditWrapper>
      <blockquote className="reddit-card" data-card-created="1607674317">
        <a href={url}>Card</a>
      </blockquote>
    </RedditWrapper>
  );
};

RedditBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

export default RedditBB;
