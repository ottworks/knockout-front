import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useAsyncScript } from './RedditBB';

export const getTikTokId = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'www.tiktok.com') throw new Error();
    const path = url.pathname.split('/');
    return path[path.length - 1];
  } catch (error) {
    return null;
  }
};

const TikTokWrapper = styled.div`
  .tiktok-embed {
    max-width: 605px;
    min-width: 325px;
    padding: 0;
    border: none;
  }
`;

const TikTokBB = ({ href, children }) => {
  useAsyncScript('https://www.tiktok.com/embed.js');
  const url = href || children.join('');
  const id = getTikTokId(url);
  return (
    <TikTokWrapper>
      <blockquote className="tiktok-embed" cite={url} data-video-id={id}>
        <section>Card</section>
      </blockquote>
    </TikTokWrapper>
  );
};

TikTokBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};

export default TikTokBB;
