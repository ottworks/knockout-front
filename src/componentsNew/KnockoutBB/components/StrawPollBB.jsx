import React, { useState } from 'react';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';

export const getStrawPollId = (href) => {
  const strawPollRegx = /^http[s]?:\/\/[www.]*strawpoll\.me\/(?:#!\/)?([0-9]+)/;
  const poll = strawPollRegx.exec(href);
  if (!poll) return null;
  const pollId = poll[1];
  if (!pollId) return null;
  return pollId;
};

const StrawPollBB = ({ href, children }) => {
  const [visible, setVisible] = useState(false);

  const onChange = (isVisible) => isVisible && setVisible(isVisible);
  const strawpollUrl = href || children.join('');
  const strawPollId = getStrawPollId(strawpollUrl);

  return visible ? (
    <iframe
      title="Poll on Straw Poll"
      src={`https://www.strawpoll.me/embed_1/${strawPollId}`}
      style={{ width: '100%', maxWidth: '680px', height: '485px' }}
    />
  ) : (
    <VisibilitySensor partialVisibility minTopValue={150} intervalDelay="250" onChange={onChange}>
      <img alt="Straw Poll placeholder" src="/static/straw_poll_placeholder.webp" />
    </VisibilitySensor>
  );
};
StrawPollBB.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default StrawPollBB;
