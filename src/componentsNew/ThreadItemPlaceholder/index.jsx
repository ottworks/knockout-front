import React from 'react';
import PropTypes from 'prop-types';
import { StyledThreadItem } from '../ThreadItem';
import Placeholder from '../Placeholder';

const ThreadItemPlaceholder = ({ minimal }) => (
  <StyledThreadItem threadOpacity={1} minimal={minimal} showTopRating={false}>
    <div className="image thread-icon">
      <Placeholder width={40} height={40} marginBottom={0} />
    </div>
    <div className="content">
      <div className="first-row">
        <Placeholder width={220} marginBottom={0} textSize={minimal ? 'large' : ''} />
      </div>
      {!minimal && (
        <div className="second-row">
          <Placeholder width={160} marginBottom={0} />
        </div>
      )}
    </div>
    <div className="info">
      {!minimal && (
        <>
          <div className="stats-container">
            <Placeholder width={100} textSize="small" marginBottom={0} />
            <Placeholder width={100} textSize="small" marginBottom={0} />
          </div>

          <div className="latest-post">
            <div className="post-info">
              <Placeholder width={80} textSize="small" marginBottom={0} />
              <Placeholder width={80} textSize="small" marginBottom={0} />
            </div>
            <Placeholder width={40} height={40} marginTop={6.4} marginBottom={6.4} />
          </div>
        </>
      )}
    </div>
  </StyledThreadItem>
);

ThreadItemPlaceholder.propTypes = {
  minimal: PropTypes.bool,
};

ThreadItemPlaceholder.defaultProps = {
  minimal: false,
};

export default ThreadItemPlaceholder;
