import styled from 'styled-components';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeTextColor,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
} from '../../utils/ThemeNew';

export const Panel = styled.div`
  position: relative;
  flex-shrink: 0;
  min-width: 320px;
  padding-bottom: ${ThemeVerticalPadding};
  margin-top: ${ThemeVerticalPadding};
  background: ${ThemeBackgroundDarker};
  transition: background 1s;
  line-height: 1.5;
`;

export const PanelTitle = styled.h2`
  position: relative;
  padding-top: ${ThemeVerticalPadding};
  padding-bottom: ${ThemeVerticalPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  margin-top: 0;
  margin-bottom: ${ThemeVerticalPadding};
  font-size: ${ThemeFontSizeMedium} !important;
  font-weight: 100;
  line-height: 1.4;
  text-align: center;
  color: ${ThemeTextColor};
  background: ${ThemeBackgroundLighter};
  transition: background 1s;
`;

export const PanelParagraph = styled.p`
  line-height: 1.3;
  color: ${ThemeTextColor};
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;
