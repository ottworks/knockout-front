import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useHistory, Link } from 'react-router-dom';
import loadable from 'loadable-components';

import { StyledHeader, HeaderLink } from './components/style';
import UserControls from './components/UserControls';
import BannedHeaderMessage from './components/BannedHeaderMessage';
import UserGroupRestricted from '../UserGroupRestricted';
import { scrollToTop } from '../../utils/pageScroll';
import LoggedInOnly from '../LoggedInOnly';

import { updateMentions } from '../../state/mentions';
import { loadBannedMessageFromStorage } from '../../utils/bannedStorage';
import { checkLoginStatus, isLoggedIn } from '../../utils/user';
import { MODERATOR_GROUPS } from '../../utils/userGroups';
import StyleableLogo from './components/StyleableLogo';
import SubscriptionsMenu from './components/SubscriptionsMenu';
import RepliesMenu from './components/RepliesMenu';
import updateSubscriptions from '../../utils/subscriptions';
import { getEventHeaderLogo, getEventText } from '../../utils/eventDates';

import MessageOfTheDay from './components/MessageOfTheDay';
import { loadPunchyLabsFromStorageBoolean } from '../../services/theme';

const LogoQuotes = loadable(() => import('./components/LogoQuotes'));

const punchyLabsEnabled = loadPunchyLabsFromStorageBoolean();

const Header = () => {
  const [bannedInformation, setBannedInformation] = useState(undefined);
  const [openReports, setOpenReports] = useState(0);
  const [ticking, setTicking] = useState(false);
  const [onTopOfPage, setOnTopOfPage] = useState(true);
  const [scrollPosition, setScrollPosition] = useState(window.scrollY);

  const dispatch = useDispatch();

  const history = useHistory();
  const location = useLocation();

  const stickyHeader = useSelector((state) => state.style.stickyHeader);

  const updateHeader = ({ mentions, subscriptions, reports }) => {
    if (subscriptions && subscriptions[0]) {
      updateSubscriptions(dispatch, subscriptions);
    }

    if (reports) {
      setOpenReports(reports || 0);
    }

    if (mentions.length > 0) {
      dispatch(updateMentions(mentions));
    }
  };

  const handleScroll = () => {
    setScrollPosition(window.scrollY);
  };

  useEffect(() => {
    if (!ticking || !scrollPosition) {
      window.requestAnimationFrame(() => {
        setOnTopOfPage(scrollPosition <= 5);
        setTicking(false);
      });
      setTicking(true);
    }
  }, [scrollPosition]);

  useEffect(() => {
    checkLoginStatus(location.pathname, (user) => updateHeader({ ...user }), history);

    if (!isLoggedIn()) {
      setBannedInformation(loadBannedMessageFromStorage());
    }

    // Add scroll listener on mount
    window.addEventListener('scroll', handleScroll, true);

    // Remove scroll listener on unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <>
      {bannedInformation && (
        <BannedHeaderMessage
          banMessage={bannedInformation.banMessage}
          threadId={bannedInformation.threadId}
        />
      )}

      <StyledHeader
        id="knockout-header"
        stickyHeader={stickyHeader}
        onTopOfPage={onTopOfPage}
        labs={punchyLabsEnabled}
      >
        <div id="header-content">
          <div className="branding">
            <Link to="/" className="brand">
              {getEventHeaderLogo() ? (
                <img src={getEventHeaderLogo()} className="header-logo" alt="Knockout logo" />
              ) : (
                <StyleableLogo className="header-logo" />
              )}
              <div className="title-container">
                <div className="title">{`${getEventText()}!`}</div>
                <LogoQuotes onTopOfPage={onTopOfPage} />
              </div>
            </Link>

            <div id="nav-items">
              <Link to="/rules" className="link">
                <i className="fas fa-atlas" />
                <span>Rules</span>
              </Link>

              <Link className="link" to="/search">
                <i className="fas fa-search" />
                <span>Search</span>
              </Link>

              <LoggedInOnly>
                <SubscriptionsMenu />

                <Link to="/events" className="link" onClick={scrollToTop}>
                  <i className="fas fa-bullhorn" />
                  <span>Events</span>
                </Link>

                <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
                  <HeaderLink
                    to="/moderate"
                    className="link"
                    onClick={() => {
                      setOpenReports(0);
                      scrollToTop();
                    }}
                  >
                    <i className="fas fa-shield-alt" />
                    <span>Moderation</span>
                    {openReports > 0 && <div className="link-notification">{openReports}</div>}
                  </HeaderLink>
                </UserGroupRestricted>
              </LoggedInOnly>
            </div>
          </div>
          <LoggedInOnly>
            <RepliesMenu />
          </LoggedInOnly>
          <UserControls />

          <div className="env-tag">{punchyLabsEnabled ? 'LABS' : 'BETA'}</div>
        </div>

        <MessageOfTheDay />

        <div className="backdrop-filter" />
      </StyledHeader>
    </>
  );
};

export default Header;
