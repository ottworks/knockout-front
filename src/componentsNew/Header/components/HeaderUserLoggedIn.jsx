import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { HashLink as Link } from 'react-router-hash-link';

import UserRoleWrapper from '../../UserRoleWrapper';
import useDropdownMenu, {
  DropdownMenuButton,
  DropdownMenuItem,
  DropdownMenuOpened,
} from './DropdownMenu';
import {
  ThemeFontSizeLarge,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import UserAvatar from './UserAvatar';

const HeaderUserLoggedIn = ({ user }) => {
  const userMenuRef = useRef();
  const userButtonRef = useRef();

  const [open, setOpen] = useDropdownMenu(userMenuRef, userButtonRef);
  return (
    <DropdownMenuButton
      className="user-menu-button"
      onClick={() => setOpen((value) => !value)}
      ref={userButtonRef}
    >
      <div className="dropdown-menu-button-inner">
        {user.avatarUrl && user.avatarUrl !== 'none.webp' && <StyledUserAvatar user={user} />}
        <UserRoleWrapper user={user}>
          <UserMenuUsername>{user.username}</UserMenuUsername>
        </UserRoleWrapper>
        <i className="fas fa-caret-down" />
      </div>
      {open && (
        <UserMenuOpened ref={userMenuRef}>
          <Link to={`/user/${user.id}`}>
            <UserMenuItem>
              <i className="fas fa-user">&nbsp;</i>
              Profile
            </UserMenuItem>
          </Link>
          <Link to="/usersettings">
            <UserMenuItem>
              <i className="fas fa-cog">&nbsp;</i>
              Settings
            </UserMenuItem>
          </Link>
          <Link to="/logout">
            <UserMenuItem>
              <i className="fas fa-sign-out-alt">&nbsp;</i>
              Log out
            </UserMenuItem>
          </Link>
        </UserMenuOpened>
      )}
    </DropdownMenuButton>
  );
};

HeaderUserLoggedIn.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    avatarUrl: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    usergroup: PropTypes.number.isRequired,
  }).isRequired,
};

const UserMenuOpened = styled(DropdownMenuOpened)`
  min-width: 200px;
`;

const UserMenuUsername = styled.span`
  margin: 0 ${ThemeHorizontalPadding};
  line-height: 1.3;
  font-size: ${ThemeFontSizeLarge};
  font-weight: bold;
  user-select: none;
`;

const StyledUserAvatar = styled(UserAvatar)`
  height: 47px;
  background: rgba(0, 0, 0, 0.15);
`;

const UserMenuItem = styled(DropdownMenuItem)`
  padding: calc(${ThemeVerticalPadding} * 1.6) calc(${ThemeHorizontalPadding} * 2);

  i {
    margin-right: 10px;
  }
`;

export default HeaderUserLoggedIn;
