import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeTextColor,
  ThemeHighlightWeaker,
  ThemeFontSizeMedium,
} from '../../utils/ThemeNew';

const Tabs = ({ currentTab, tabs, setTab, centered }) => {
  return (
    <StyledTabs centered={centered}>
      {tabs.map((tab, index) => (
        <button
          key={tab}
          onClick={() => setTab(index)}
          className={`tab ${currentTab === index ? 'current' : ''}`}
          type="button"
        >
          {tab}
        </button>
      ))}
    </StyledTabs>
  );
};

Tabs.propTypes = {
  currentTab: PropTypes.number.isRequired,
  tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
  setTab: PropTypes.func.isRequired,
  centered: PropTypes.bool,
};

Tabs.defaultProps = {
  centered: false,
};

const tabHeight = '50px';

export const StyledTabs = styled.div`
  width: 100%;
  height: ${tabHeight};
  display: flex;
  ${(props) => props.centered && 'justify-content: space-around;'}
  margin-bottom: 25px;
  .tab {
    position: relative;
    text-transform: capitalize;
    font-size: ${ThemeFontSizeMedium};
    background-color: transparent;
    cursor: pointer;
    border: none;
    color: ${ThemeTextColor};
    height: ${tabHeight};
    line-height: ${tabHeight};
    padding: 0 calc(${ThemeHorizontalPadding} * 2);
    &:before,
    &:after {
      content: ' ';
      position: absolute;
      height: 2px;
      width: 0%;
      background-color: ${ThemeHighlightWeaker};
      bottom: 0;
      transition: width 200ms ease-in-out;
    }
    &:before {
      right: 50%;
    }
    &:after {
      left: 50%;
    }
    &:focus,
    &:hover {
      outline: none;
      &:before,
      &:after {
        width: 25%;
      }
    }
    &.current {
      &:after,
      &:before {
        content: ' ';
        position: absolute;
        height: 2px;
        width: 50%;
      }
    }
  }

  @media (max-width: 960px) {
    justify-content: space-around;
  }

  @media (max-width: 450px) {
    margin-bottom: calc(${ThemeVerticalPadding} * 3);
    flex-direction: column;
    height: auto;
    position: relative;
    align-items: flex-start;
    button {
      display: block;
    }
  }
`;

export default Tabs;
