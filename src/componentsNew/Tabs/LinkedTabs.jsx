import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { StyledTabs } from '.';

const LinkedTabs = ({ tabs, centered }) => {
  return (
    <StyledTabs centered={centered}>
      {tabs.map((tab) => (
        <NavLink
          exact={tab.exact}
          key={tab.name}
          to={tab.path}
          className="tab"
          activeClassName="current"
        >
          {tab.name}
        </NavLink>
      ))}
    </StyledTabs>
  );
};

LinkedTabs.propTypes = {
  tabs: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
      exact: PropTypes.bool,
    })
  ).isRequired,
  centered: PropTypes.bool,
};

LinkedTabs.defaultProps = {
  centered: false,
};

export default LinkedTabs;
