import axios from 'axios';

import config from '../../config';

export const getLatestThreads = async () => {
  const res = await axios.get(`${config.apiHost}/thread/latest`);
  const { data } = res;
  return data;
};
