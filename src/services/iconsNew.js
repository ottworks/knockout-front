import icons from '../../config/icons';
import { usergroupCheck } from '../componentsNew/UserGroupRestricted';
import { MODERATOR_GROUPS } from '../utils/userGroups';

export function listIcons() {
  const showAdminIcons = usergroupCheck(MODERATOR_GROUPS);

  return icons
    .filter((icon) => (showAdminIcons ? true : icon.restricted !== true))
    .map((icon) => ({
      id: icon.id,
      url: icon.url,
      desc: icon.description,
      category: icon.category,
    }));
}

export function getIcon(id) {
  const icon = icons[id] || icons[0];
  return { id, url: icon.url, desc: icon.description };
}
