import { authGet, authPost } from './common';
import { pushNotification, pushSmartNotification } from '../utils/notification';

export const getOpenReports = async () => {
  const res = await authGet({ url: `/reports?open=true` });
  const { data } = res;

  return data.reports;
};

export const getReports = async () => {
  const res = await authGet({ url: `/reports` });
  const { data } = res;

  return data.reports;
};

export const getReport = async (id) => {
  const res = await authGet({ url: `/reports/${id}` });
  const { data } = res;

  return data;
};

const submitReport = async ({ postId, reportReason }) => {
  if (!postId || !reportReason || reportReason.length < 3) {
    throw Error('Missing required info.');
  }

  const requestBody = {
    postId,
    reportReason,
  };

  const response = await authPost({ url: '/reports', data: requestBody });

  return pushSmartNotification(response.data);
};

export const resolveReport = async (reportId) => {
  const res = await authPost({ url: `/reports/${reportId}/resolve` });

  const { data } = res;

  pushNotification({ message: 'Report marked as solved.', type: 'success' });

  return data;
};

export default submitReport;
