import axios from 'axios';

import config from '../../config';

const getEventsList = async () => {
  const res = await axios.get(`${config.apiHost}/events`);

  const { data } = res;

  return data;
};

export default getEventsList;
