import axios from 'axios';

import config from '../../config';

export const getPopularThreads = async () => {
  const res = await axios.get(`${config.apiHost}/thread/popular`);
  const { data } = res;
  return data;
};
