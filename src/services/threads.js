import axios from 'axios';

import config from '../../config';

import { authPost, authGet, authPut } from './common';
import { pushSmartNotification } from '../utils/notification';

import { POST_CHARACTER_LIMIT } from '../utils/limits';

export const getThreadList = async () => {
  const res = await axios.get(`${config.apiHost}/thread`);

  const { data } = res;

  return data;
};

export const getThread = async (threadId) => {
  const res = await axios.get(`${config.apiHost}/thread/${threadId}`, {
    headers: { 'Cache-Control': 'private, no-store, max-age=0' },
  });

  const { data } = res;

  return data;
};

export const getThreadWithPosts = async (threadId, page = 1) => {
  const user = localStorage.getItem('currentUser');

  // if logged in, get the subforum with auth so
  // subscribed threads are fetched
  if (user) {
    const res = await authGet({
      url: `/thread/${threadId}/${page}`,
      headers: { 'Cache-Control': 'private, no-store, max-age=0' },
    });

    const { data } = res;

    return data;
  }
  try {
    const res = await axios.get(`${config.apiHost}/thread/${threadId}/${page}`);

    const { data } = res;

    if (res.status !== 200) {
      return {
        title: 'Thread not found',
      };
    }

    return data;
  } catch (err) {
    return {
      title: 'Thread not found',
      posts: [],
      subforumId: 1,
    };
  }
};

export const createNewThread = async (data) => {
  if (data.title.length < 3) {
    throw new Error({ error: 'Title too short.' });
  }
  if (data.title.length > 140) {
    throw new Error({ error: 'Title too long.' });
  }

  const contentSerialized = data.content;
  const contentLength = contentSerialized.trim().length;

  const contentStringified = data.content;

  if (contentLength > POST_CHARACTER_LIMIT) {
    throw new Error({ error: `Thread body must be under ${POST_CHARACTER_LIMIT} characters.` });
  }
  if (contentLength < 1) {
    throw new Error({ error: 'Post body too short.' });
  }

  const thread = await authPost({
    url: '/thread',
    data: { ...data, content: contentStringified, postContent: data.content },
  });

  return thread.data;
};

export const updateThread = async (data) => {
  // add to this so only the fields we want can be submitted
  if (!data.title && !data.subforum_id && data.backgroundUrl == null) {
    pushSmartNotification({ error: 'No valid update data provided.' });
    throw new Error({ error: 'No valid update data provided.' });
  }

  if (data.title && (data.title.length < 3 || data.title.length > 140 || data.title === '')) {
    pushSmartNotification({ error: 'Invalid title.' });
    throw new Error({ error: 'Invalid title.' });
  }

  const thread = await authPut({ url: '/thread', data });

  return thread.data;
};
