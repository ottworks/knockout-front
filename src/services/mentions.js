import { authPost, authPut } from './common';

export const getMentions = async () => {
  const user = localStorage.getItem('currentUser');

  if (!user) {
    return [];
  }
  try {
    const results = await authPost({ url: '/mentions/get', data: {} });

    return results.data;
  } catch (err) {
    console.error(err);
    return [];
  }
};

export const updateMention = async postIds => {
  const user = localStorage.getItem('currentUser');

  if (!user || !postIds) {
    return [];
  }
  try {
    const results = await authPut({ url: '/mentions', data: { postIds } });

    return results.data;
  } catch (err) {
    console.error(err);
    return [];
  }
};

export const getMentionsRequest = async () => authPost({ url: '/mentions/get', data: {} });
