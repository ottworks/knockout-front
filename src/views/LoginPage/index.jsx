/* eslint-disable react/jsx-one-expression-per-line */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { saveUserToStorage } from '../../services/user';

import LoginButton from '../../componentsNew/LoginButton';
import { Panel, PanelTitle, PanelParagraph } from '../../componentsNew/Panel';

const LoginPage = () => {
  const [error, setError] = useState(null);

  const onLogin = (data) => {
    setError(data.error);
    if (data.user) {
      saveUserToStorage(data);
      window.location = '/';
    }
  };

  /**
   * @param {MessageEvent} message
   */
  const messageCallback = (message) => {
    if (message.data.isLoginCallback) {
      onLogin(message.data);
    }
  };

  useEffect(() => {
    window.addEventListener('message', messageCallback);
    return () => window.removeEventListener('message', messageCallback);
  }, []);

  return (
    <StyledLoginPage>
      <ErrorPanel visible={error != null}>
        <ErrorPanelTitle>Error</ErrorPanelTitle>
        <PanelParagraph>{error}</PanelParagraph>
      </ErrorPanel>
      <div className="main-container">
        <div className="column">
          <Panel>
            <PanelTitle>Heads up!</PanelTitle>
            <PanelParagraph>
              The buttons on the right will create an account for you. Before you do that, make sure
              you&apos;re aware of and agree with the{' '}
              <Link className="panel-link" to="/rules/privacy-policy">
                privacy policy / terms of service
              </Link>
              . You <b>must</b> be 18 or older to sign up.
            </PanelParagraph>
          </Panel>
          <Panel>
            <PanelTitle>Having trouble logging in?</PanelTitle>
            <PanelParagraph>
              If you can&apos;t log in, make sure that you are not blocking the cookies. Check your
              browser&apos;s settings. You might need to either allow third-party cookies or make an
              exception for <CodeBlock>knockout.chat</CodeBlock>, and possibly{' '}
              <CodeBlock>accounts.google.com</CodeBlock>. If that wasn&apos;t it, you might need to
              check your extensions, and make exceptions if applicable. Hop on the Knockout Discord
              for help if you can&apos;t figure it out on your own.
            </PanelParagraph>
          </Panel>
        </div>
        <div className="column right">
          <Panel>
            <PanelTitle>Login Providers</PanelTitle>
            <LoginButton color="white" background="#d14836" route="/auth/google/login">
              <i className="fab fa-google" />
              <span>Sign up / Log in with Google</span>
            </LoginButton>
            <LoginButton color="white" background="#1da1f2" route="/auth/twitter/login">
              <i className="fab fa-twitter" />
              <span>Sign up / Log in with Twitter</span>
            </LoginButton>
            <LoginButton color="white" background="#24292e" route="/auth/github/login">
              <i className="fab fa-github" />
              <span>Sign up / Log in with GitHub</span>
            </LoginButton>
            <LoginButton color="white" background="#171a21" route="/auth/steam/login">
              <i className="fab fa-steam" />
              <span>Sign up / Log in with Steam</span>
            </LoginButton>
          </Panel>
        </div>
      </div>
    </StyledLoginPage>
  );
};

export default LoginPage;

const StyledLoginPage = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;

  .main-container {
    display: flex;
    flex-direction: row;

    @media (max-width: 700px) {
      flex-wrap: wrap;
    }
  }

  .column {
    display: flex;
    flex-direction: column;
    flex-shrink: 1;
    flex-grow: 0;
    padding: 0px 6px;
  }

  .panel-link {
    color: #3facff;
  }
`;

const CodeBlock = styled.span`
  display: inline-block;
  background: rgba(0, 0, 0, 0.25);
  font-family: monospace;
  white-space: pre-line;
  margin: 0;
  padding: 0 5px;
`;

const ErrorPanel = styled(Panel)`
  display: ${(props) => (props.visible ? 'block' : 'none')};
  background-color: maroon;
  text-align: center;
  font-weight: bold;
  color: white;
`;

const ErrorPanelTitle = styled(PanelTitle)`
  background-color: red;
  font-weight: bold;
`;
