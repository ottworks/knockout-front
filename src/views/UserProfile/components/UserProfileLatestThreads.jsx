import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../../../componentsNew/Pagination';
import ThreadItem from '../../../componentsNew/ThreadItem';
import ThreadItemPlaceholder from '../../../componentsNew/ThreadItemPlaceholder';

const UserProfileLatestThreads = ({ threads, user, loading, pageChangeFn }) => {
  let threadContent = Array(16)
    .fill(1)
    // eslint-disable-next-line react/no-array-index-key
    .map((item, index) => <ThreadItemPlaceholder minimal key={`p${index}`} />);
  if (!loading)
    threadContent = threads.threads.map((thread) => (
      <ThreadItem
        key={thread.id}
        id={thread.id}
        createdAt={thread.createdAt}
        deleted={thread.deleted}
        iconId={thread.iconId}
        locked={thread.locked}
        pinned={thread.pinned}
        postCount={thread.postCount}
        title={thread.title}
        user={user}
        tags={thread.tags}
        backgroundUrl={thread.backgroundUrl}
        backgroundType={thread.backgroundType}
        subforumName={thread.subforumName}
        minimal
      />
    ));
  return (
    <div>
      <Pagination
        pageChangeFn={pageChangeFn}
        totalPosts={threads.totalThreads}
        currentPage={threads.currentPage}
        pageSize={40}
        marginBottom
        useButtons
      />
      {threadContent}
      <Pagination
        pageChangeFn={pageChangeFn}
        totalPosts={threads.totalThreads}
        currentPage={threads.currentPage}
        pageSize={40}
        useButtons
      />
    </div>
  );
};

UserProfileLatestThreads.propTypes = {
  threads: PropTypes.shape({
    totalThreads: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
    threads: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  loading: PropTypes.bool.isRequired,
  pageChangeFn: PropTypes.func.isRequired,
};

export default UserProfileLatestThreads;
