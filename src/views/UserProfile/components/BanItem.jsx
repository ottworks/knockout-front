import React from 'react';
import dayjs from 'dayjs';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import relativeTime from 'dayjs/plugin/relativeTime';
import { darken, desaturate } from 'polished';
import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../../Theme';
import {
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeHuge,
  ThemeBannedUserColor,
} from '../../../utils/ThemeNew';

dayjs.extend(relativeTime);

const toDate = (date) =>
  new Intl.DateTimeFormat(undefined, {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }).format(new Date(date));

const BanItem = ({
  banReason,
  createdAt,
  expiresAt,
  duration,
  bannedBy,
  banStillValid,
  isMod,
  invalidateBan,
  thread,
  post,
}) => (
  <StyledBan>
    <div className="ban-container">
      <div className="ban-context">
        <div className="ban-expiration">
          {dayjs(expiresAt).isBefore(dayjs().year(2005))
            ? 'Reverted'
            : (banStillValid ? 'Expires ' : 'Expired ') + dayjs(expiresAt).fromNow()}
        </div>
        <div className="ban-reason">{`"${banReason}"`}</div>
        <div className="ban-details" title={toDate(createdAt)}>
          {dayjs(createdAt).isAfter(dayjs().year(2005)) && dayjs(createdAt).fromNow()}
          {post && thread && (
            <span>
              &nbsp;in&nbsp;
              <Link
                to={`/thread/${thread.id}/${post.page}#post-${post.id}`}
                className="ban-details-thread"
              >
                {thread.title}
              </Link>
            </span>
          )}
        </div>
      </div>
      <div className="ban-footer">
        <div className="ban-info">
          <div className="banned-by">
            by&nbsp;
            <span className="banned-by-user">{bannedBy.username}</span>
          </div>
          <div className="ban-duration">
            <i className="fas fa-clock ban-duration-icon" />
            {duration}
          </div>
        </div>
        {banStillValid && isMod && (
          <button className="unban-button" type="button" onClick={invalidateBan}>
            Unban
          </button>
        )}
      </div>
    </div>
  </StyledBan>
);

export default BanItem;

BanItem.propTypes = {
  banReason: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  duration: PropTypes.string.isRequired,
  bannedBy: PropTypes.shape({
    username: PropTypes.string.isRequired,
  }).isRequired,
  banStillValid: PropTypes.bool.isRequired,
  isMod: PropTypes.bool.isRequired,
  invalidateBan: PropTypes.func.isRequired,
  thread: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
  }),
  post: PropTypes.shape({
    page: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
  }),
  expiresAt: PropTypes.string.isRequired,
};

BanItem.defaultProps = {
  thread: undefined,
  post: undefined,
};

const StyledBan = styled.div`
  background: ${ThemeBannedUserColor};
  color: white;

  transition: background 200ms ease-in-out;

  display: inline-block;
  vertical-align: top;
  min-width: 240px;
  width: calc(25% - 39px);
  height: 350px;
  margin-right: 10px;
  margin-top: 10px;

  position: relative;
  overflow: hidden;

  &:nth-child(4n) {
    margin-right: 0;
  }
  &:nth-child(4n - 7) {
    margin-left: 0;
  }

  .ban-expiration {
    text-align: center;
    font-size: ${ThemeFontSizeMedium};
    padding: 10px 0;
    background: ${(props) => desaturate(0.2, darken(0.1, ThemeBannedUserColor(props)))};
  }

  .ban-container {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 100%;
  }

  .ban-reason {
    margin: 10px 0px;
    font-weight: bold;
    font-size: ${ThemeFontSizeHuge};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  }

  .ban-details {
    font-size: ${ThemeFontSizeLarge};
    line-height: 1.25rem;
    padding: 0 ${ThemeHorizontalPadding};
  }

  .ban-details-thread {
    font-weight: bold;
  }

  .banned-by {
    max-width: 150px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    line-height: normal;

    .banned-by-user {
      font-weight: bold;
    }
  }

  .ban-info {
    display: flex;
    align-items: center;
    justify-content: space-between;
    font-size: ${ThemeFontSizeMedium};
    margin-bottom: 7px;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  }

  .ban-duration {
    display: flex;
    align-items: center;
  }

  .ban-duration-icon {
    font-size: ${ThemeFontSizeLarge};
    margin-right: 5px;
  }

  .unban-button {
    width: 100%;
    background: #b53329;
    color: #ff9a92;
    border: none;
    height: 36px;

    &:hover {
      cursor: pointer;
      filter: brightness(1.1);
    }
  }

  @media (max-width: 960px) {
    width: 100%;
    max-width: 240px;
    box-sizing: border-box;

    margin: 0;
    margin-bottom: 10px;

    &:nth-child(4n) {
      margin-right: unset;
    }
    &:nth-child(4n - 7) {
      margin-left: unset;
    }
  }
`;
