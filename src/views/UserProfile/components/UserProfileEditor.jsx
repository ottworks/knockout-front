import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { updateUserProfile } from '../../../services/user';

import Modal from '../../../componentsNew/Modals/Modal';
import ModalRadioButton from '../../../componentsNew/Modals/ModalRadioButton';
import { FieldLabelSmall, TextField } from '../../../componentsNew/FormControls';

const UserProfileEditor = ({ closeFn, isOpen, profile, callback }) => {
  const [headingText, setHeadingText] = useState('');
  const [personalSite, setPersonalSite] = useState('');
  const [backgroundType, setBackgroundType] = useState('cover');
  const [backgroundImage, setBackgroundImage] = useState(null);

  useEffect(() => {
    setHeadingText(profile.headingText || '');
    setPersonalSite(profile.personalSite || '');
    setBackgroundType(profile.backgroundType || '');
  }, [profile]);

  const handleSubmit = async () => {
    await updateUserProfile({
      headingText: headingText || '',
      personalSite: personalSite || '',
      backgroundType,
      backgroundImage,
    });
    callback({ headingText, personalSite, backgroundType, backgroundImage });
    closeFn();
  };

  return (
    <Modal
      iconUrl="https://img.icons8.com/color/96/000000/paint-net.png"
      title="Edit your profile"
      cancelFn={closeFn}
      submitFn={handleSubmit}
      isOpen={isOpen}
    >
      <FieldLabelSmall>Heading text</FieldLabelSmall>
      <TextField value={headingText} onChange={(e) => setHeadingText(e.target.value)} />
      <FieldLabelSmall>Personal site URL</FieldLabelSmall>
      <TextField value={personalSite} onChange={(e) => setPersonalSite(e.target.value)} />
      <FieldLabelSmall>Background type</FieldLabelSmall>
      <ModalRadioButton
        name="backgroundType"
        property={backgroundType}
        values={['cover', 'tiled']}
        onChange={(e) => setBackgroundType(e.target.value)}
      />
      <FieldLabelSmall>Background image (max size ~2MB)</FieldLabelSmall>
      <input
        name="backgroundImage"
        type="file"
        onChange={(e) => setBackgroundImage(e.target.files[0])}
      />
    </Modal>
  );
};

UserProfileEditor.propTypes = {
  closeFn: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  profile: PropTypes.shape({
    headingText: PropTypes.string,
    personalSite: PropTypes.string,
    backgroundType: PropTypes.string,
  }).isRequired,
  callback: PropTypes.func.isRequired,
};
export default UserProfileEditor;
