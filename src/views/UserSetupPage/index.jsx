import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import StyleWrapper from '../RulesPage/components/StyleWrapper';
import PrivacyPolicy from '../RulesPage/components/PrivacyPolicy';
import Rules from '../RulesPage/components/Rules';
import UsernameForm from './components/UsernameForm';
import PageSwitcher from './components/PageSwitcher';

import { updateUser } from '../../services/user';
import { userLogin } from '../../state/user/actions';

import { ThemeHorizontalPadding } from '../../utils/ThemeNew';

const submitUsername = async ({ username, user, history, dispatch }) => {
  try {
    await updateUser({ username });
    const updatedUser = { ...user, username, requiresSetup: false };
    userLogin(updatedUser, history, dispatch);
    return true;
  } catch (err) {
    console.error(err);
    return false;
  }
};

const UserSetup = ({ user, history }) => {
  const [page, setPage] = useState(1);
  if (!user.requiresSetup) {
    return <Redirect to="/" />;
  }

  const nextPage = () => {
    setPage(page + 1);
    document.activeElement.blur();
    window.scrollTo({
      top: 0,
      left: 0,
    });
  };

  const prevPage = () => {
    setPage(page - 1);
    document.activeElement.blur();
    window.scrollTo({
      top: 0,
      left: 0,
    });
  };

  const submitUser = async (username, dispatch) => {
    return submitUsername({ username, user, history, dispatch });
  };

  const Page = () => {
    switch (page) {
      case 1:
        return (
          <StyleWrapper>
            <PrivacyPolicy />
          </StyleWrapper>
        );
      case 2:
        return (
          <StyleWrapper>
            <Rules interactable={false} />
          </StyleWrapper>
        );
      case 3:
        return (
          <StyleWrapper>
            <UsernameForm submitUsername={submitUser} />
          </StyleWrapper>
        );
      default: {
        setPage(1);
        return (
          <StyleWrapper>
            <PrivacyPolicy />
          </StyleWrapper>
        );
      }
    }
  };

  return (
    <StyledUserSetup>
      <Helmet>
        <title>User Setup - Knockout!</title>
      </Helmet>
      {Page()}
      <PageSwitcher currentPage={page} pageAmount={3} nextPage={nextPage} prevPage={prevPage} />
    </StyledUserSetup>
  );
};

UserSetup.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string,
    requiresSetup: PropTypes.bool,
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

UserSetup.defaultProps = {
  user: {
    username: undefined,
    requiresSetup: false,
  },
};

const StyledUserSetup = styled.div`
  padding: calc(${ThemeHorizontalPadding} * 3);
`;

const mapStateToProps = ({ user }) => ({
  user,
});

export default withRouter(connect(mapStateToProps)(UserSetup));
