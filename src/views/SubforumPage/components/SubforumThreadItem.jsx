import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ThreadItem from '../../../componentsNew/ThreadItem';

import { deleteReadThread } from '../../../services/readThreads';
import { pushNotification } from '../../../utils/notification';

const SubforumThreadItem = ({
  id,
  createdAt,
  deleted,
  iconId,
  lastPost,
  locked,
  pinned,
  postCount,
  read,
  readThreadUnreadPosts,
  title,
  unreadPostCount,
  user,
  firstPostTopRating,
  firstUnreadId,
  tags,
  backgroundUrl,
  backgroundType,
  subforumName,
}) => {
  let unreadPosts = null;
  if (unreadPostCount > 0) {
    unreadPosts = unreadPostCount;
  }
  if (readThreadUnreadPosts > 0) {
    unreadPosts = readThreadUnreadPosts;
  }
  const [isRead, setIsRead] = useState(read);
  const threadOpacity = isRead && readThreadUnreadPosts === 0 ? '0.5' : '1.0';

  const markUnread = async () => {
    try {
      await deleteReadThread(id);
      setIsRead(false);
      pushNotification({ message: 'Thread marked as unread.' });
    } catch (err) {
      pushNotification({ message: 'Could not mark thread as unread.', type: 'error' });
    }
  };

  return (
    <ThreadItem
      id={id}
      createdAt={createdAt}
      deleted={deleted}
      iconId={iconId}
      lastPost={lastPost}
      locked={locked}
      pinned={pinned}
      postCount={postCount}
      threadIsRead={isRead}
      title={title}
      unreadPostCount={unreadPosts}
      user={user}
      showTopRating
      firstPostTopRating={firstPostTopRating}
      firstUnreadId={firstUnreadId}
      tags={tags}
      backgroundUrl={backgroundUrl}
      backgroundType={backgroundType}
      markUnreadAction={markUnread}
      threadOpacity={threadOpacity}
      subforumName={subforumName}
    />
  );
};

SubforumThreadItem.propTypes = {
  id: PropTypes.number.isRequired,
  createdAt: PropTypes.string.isRequired,
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string.isRequired,
  }).isRequired,
  deleted: PropTypes.bool,
  iconId: PropTypes.number.isRequired,
  lastPost: PropTypes.shape({
    id: PropTypes.number.isRequired,
    createdAt: PropTypes.string.isRequired,
    user: PropTypes.shape({
      avatarUrl: PropTypes.string,
      username: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  locked: PropTypes.bool,
  pinned: PropTypes.bool,
  postCount: PropTypes.number.isRequired,
  read: PropTypes.bool,
  readThreadUnreadPosts: PropTypes.number,
  title: PropTypes.string.isRequired,
  unreadPostCount: PropTypes.number,
  firstPostTopRating: PropTypes.shape({
    count: PropTypes.number.isRequired,
    rating: PropTypes.string.isRequired,
  }),
  firstUnreadId: PropTypes.number,
  tags: PropTypes.arrayOf(PropTypes.object),
  backgroundUrl: PropTypes.string,
  backgroundType: PropTypes.string,
  subforumName: PropTypes.string,
};

SubforumThreadItem.defaultProps = {
  deleted: false,
  locked: false,
  pinned: false,
  read: false,
  readThreadUnreadPosts: 0,
  unreadPostCount: 0,
  firstPostTopRating: null,
  firstUnreadId: 0,
  tags: [{}],
  backgroundUrl: '',
  backgroundType: '',
  subforumName: '',
};

export default SubforumThreadItem;
