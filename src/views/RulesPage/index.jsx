import React from 'react';
import styled from 'styled-components';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import Rules from './components/Rules';
import PrivacyPolicy from './components/PrivacyPolicy';
import ModeratorCoc from './components/ModeratorCoc';
import StyleWrapper from './components/StyleWrapper';
import { ThemeHorizontalPadding } from '../../utils/ThemeNew';
import LinkedTabs from '../../componentsNew/Tabs/LinkedTabs';

const RulesPage = () => {
  const { path } = useRouteMatch();

  const tabs = [
    { name: 'Rules', path, exact: true },
    { name: 'Privacy Policy', path: `${path}/privacy-policy` },
    { name: 'Moderator Code of Conduct', path: `${path}/moderator-coc` },
  ];

  return (
    <StyleWrapper>
      <StyledRulesPage>
        <LinkedTabs tabs={tabs} centered />
        <Switch>
          <Route exact path={path}>
            <Rules />
          </Route>
          <Route path={`${path}/privacy-policy`}>
            <PrivacyPolicy />
          </Route>
          <Route path={`${path}/moderator-coc`}>
            <ModeratorCoc />
          </Route>
        </Switch>
      </StyledRulesPage>
    </StyleWrapper>
  );
};

const StyledRulesPage = styled.div`
  padding: calc(${ThemeHorizontalPadding} * 3);
  padding-top: 0;
  position: relative;
  @media (max-width: 450px) {
    padding-top: calc(${ThemeHorizontalPadding} * 3);
  }
`;

export default RulesPage;
