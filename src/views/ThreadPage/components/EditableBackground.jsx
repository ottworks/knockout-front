import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useSelector, useDispatch } from 'react-redux';
import LoggedInOnly from '../../../componentsNew/LoggedInOnly';
import { updateThread } from '../../../services/threads';
import { pushNotification } from '../../../utils/notification';
import Tooltip from '../../../componentsNew/Tooltip';
import { SubHeaderButton } from '../../../componentsNew/Buttons';
import Modal from '../../../componentsNew/Modals/Modal';
import { updateBackgroundRequest } from '../../../state/background';
import { FieldLabelSmall, TextField } from '../../../componentsNew/FormControls';
import ModalRadioButton from '../../../componentsNew/Modals/ModalRadioButton';

const EditableBackground = ({ backgroundUrl, backgroundType, threadId, byCurrentUser }) => {
  const [currentBackground, setBackground] = useState('');
  const [currentBackgroundType, setBackgroundType] = useState('');
  const [newBackground, setNewBackground] = useState('');
  const [newBackgroundType, setNewBackgroundType] = useState('');
  const [modalOpen, setModalOpen] = useState(false);
  const usergroup = useSelector((state) => state.user.usergroup);
  const dispatch = useDispatch();

  useEffect(() => {
    setBackground(backgroundUrl);
    setBackgroundType(backgroundType);
  }, [backgroundUrl, backgroundType]);

  const openModal = () => {
    setModalOpen(true);
    setNewBackground(currentBackground);
    setNewBackgroundType(currentBackgroundType);
  };

  const edit = async () => {
    try {
      if (newBackground === null) {
        throw new Error({ error: 'Empty background' });
      }

      await updateThread({
        backgroundUrl: newBackground,
        backgroundType: newBackgroundType,
        id: threadId,
      });
      setBackground(newBackground);
      setBackgroundType(newBackgroundType);
      dispatch(updateBackgroundRequest(newBackground, newBackgroundType));
      pushNotification({ message: 'Thread background updated.' });
      setModalOpen(false);
    } catch (err) {
      console.error(err);
      pushNotification({ message: 'Error updating thread background.', type: 'error' });
    }
  };

  let button = null;

  if ((usergroup >= 3 && usergroup <= 5) || (usergroup === 2 && byCurrentUser)) {
    button = (
      <li className="subheader-dropdown-list-item">
        <Tooltip text="Change background">
          <SubHeaderButton onClick={openModal} title="Change background">
            <i className="fas fa-image" />
            <span>Change background</span>
          </SubHeaderButton>
        </Tooltip>
      </li>
    );
  } else if (byCurrentUser) {
    button = (
      <li className="subheader-dropdown-list-item" style={{ opacity: 0.5 }}>
        <Tooltip text="Change background (Must be a Gold member)">
          <SubHeaderButton title="Change background (Must be a Gold member)">
            <i className="fas fa-image" />
            <span>Change background (Must be a Gold member)</span>
          </SubHeaderButton>
        </Tooltip>
      </li>
    );
  }
  return (
    <LoggedInOnly>
      {button}
      <Modal
        iconUrl="/static/icons/image-file.png"
        title="Change background"
        cancelFn={() => setModalOpen(false)}
        submitFn={edit}
        isOpen={modalOpen}
      >
        <FieldLabelSmall>Background URL</FieldLabelSmall>
        <TextField value={newBackground} onChange={(e) => setNewBackground(e.target.value)} />
        <ModalRadioButton
          name="backgroundType"
          property={newBackgroundType}
          values={['cover', 'tiled']}
          onChange={(e) => setNewBackgroundType(e.target.value)}
        />
      </Modal>
    </LoggedInOnly>
  );
};

EditableBackground.propTypes = {
  backgroundUrl: PropTypes.string,
  backgroundType: PropTypes.string,
  byCurrentUser: PropTypes.bool,
  threadId: PropTypes.number,
};
EditableBackground.defaultProps = {
  backgroundUrl: '',
  backgroundType: 'cover',
  byCurrentUser: false,
  threadId: 0,
};

export default EditableBackground;
