import styled, { createGlobalStyle, css } from 'styled-components';
import { MobileMediaQuery } from '../../componentsNew/SharedStyles';
import { HEADER_HEIGHT, MOTD_HEIGHT, MOTD_HEIGHT_MOBILE } from '../../utils/pageScroll';
import {
  ThemeBodyWidth,
  ThemeMainBackgroundColor,
  ThemeTextColor,
  ThemeBodyBackgroundColor,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundDarker,
  ThemeFontSizeMedium,
} from '../../utils/ThemeNew';

export const FlexWrapper = styled.main`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  padding-top: calc(
    ${(props) => HEADER_HEIGHT + Number(props.hasMotd) * MOTD_HEIGHT}px + ${ThemeVerticalPadding}
  );
  ${MobileMediaQuery} {
    padding-top: calc(
      ${(props) => HEADER_HEIGHT + Number(props.hasMotd) * MOTD_HEIGHT_MOBILE}px +
        ${ThemeVerticalPadding}
    );
  }
`;

export const Background = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-image: ${(props) => (props.url ? `url("${props.url}")` : '')};
  background-position: top center;
  background-size: ${(props) => (props.bgType === 'tiled' ? 'unset' : 'cover')};
  background-repeat: ${(props) => (props.bgType === 'tiled' ? 'repeat' : 'unset')};
  z-index: -1000;
  opacity: 0.5;
`;

const NotificationStyles = css`
  .Toastify__toast-container {
    z-index: 9999;
    position: fixed;
    padding: 0;
    margin: 0 auto;
    width: 100%;
    max-width: ${ThemeBodyWidth};
    box-sizing: border-box;
    color: ${ThemeTextColor};
    pointer-events: none;
  }
  .Toastify__toast-container--top-right {
    top: -${ThemeVerticalPadding};
    padding-top: ${ThemeVerticalPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
  }

  .Toastify__toast-container--bottom-right {
    bottom: 1em;
    right: 1em;
  }

  @media only screen and (max-width: 480px) {
    .Toastify__toast-container {
      background: rgba(0, 0, 0, 0.5);
      box-shadow: 0 0 30px rgba(0, 0, 0, 1);
      border-radius: 5px;
      padding: 0;
      left: 0;
      margin: 0;
    }
    .Toastify__toast-container--bottom-left,
    .Toastify__toast-container--bottom-center,
    .Toastify__toast-container--bottom-right {
      bottom: 45px;
    }
    .Toastify__toast-container--rtl {
      right: 0;
      left: initial;
    }
  }
  .Toastify__toast {
    position: relative;
    box-sizing: border-box;
    margin-top: ${ThemeVerticalPadding};
    margin-bottom: ${ThemeVerticalPadding};
    padding: 15px;
    border-radius: 5px;
    box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.1), 0 2px 15px 0 rgba(0, 0, 0, 0.05);
    display: flex;
    width: 320px;
    right: 0;
    margin-left: auto;
    justify-content: space-between;
    max-height: 200px;
    overflow: hidden;
    font-family: 'Open Sans', sans-serif;
    cursor: pointer;
    direction: ltr;
    background: ${ThemeBackgroundDarker};
    pointer-events: all;
    min-height: 50px;
  }
  .Toastify__toast--default {
    border-bottom: #fff;
    color: #aaa;
  }

  .Toastify__toast--error {
    background: #e74c3c;
  }
  .Toastify__toast-body {
    margin: auto 0;
    flex: 1;
    font-size: ${ThemeFontSizeMedium};
  }
  @media only screen and (max-width: 480px) {
    .Toastify__toast {
      width: auto;
    }
  }
  .Toastify__close-button {
    color: #fff;
    font-weight: 700;
    font-size: 14px;
    background: 0 0;
    outline: 0;
    border: none;
    padding: 0;
    cursor: pointer;
    opacity: 0.7;
    transition: 0.3s ease;
    align-self: flex-start;
  }
  .Toastify__close-button--default {
    color: #000;
    opacity: 0.3;
  }
  .Toastify__close-button:focus,
  .Toastify__close-button:hover {
    opacity: 1;
  }
  @keyframes Toastify__trackProgress {
    0% {
      transform: scaleX(1);
    }
    100% {
      transform: scaleX(0);
    }
  }
  .Toastify__progress-bar {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 2px;
    z-index: 9999;
    opacity: 0.7;
    background: blue;
    transform-origin: left;
  }
  .Toastify__progress-bar--animated {
    animation: Toastify__trackProgress linear 1 forwards;
  }
  .Toastify__progress-bar--rtl {
    right: 0;
    left: initial;
    transform-origin: right;
  }
  .Toastify__progress-bar--info {
    background: #3498db;
  }
  .Toastify__progress-bar--success {
    background: #6dff3e;
  }
  .Toastify__progress-bar--warning {
    background: #f1c40f;
  }
  .Toastify__progress-bar--error {
    background: #e74c3c;
  }
  @keyframes Toastify__slideInRight {
    from {
      transform: translate3d(110%, 0, 0);
      opacity: 0;
      visibility: visible;
    }
    to {
      transform: translate3d(0, 0, 0);
      opacity: 1;
    }
  }
  @keyframes Toastify__slideOutRight {
    from {
      transform: translate3d(0, 0, 0);
      opacity: 1;
    }
    to {
      visibility: hidden;
      transform: translate3d(500%, 0, 0);
      opacity: 0;
    }
  }
  .Toastify__slide-enter--top-right,
  .Toastify__slide-enter--bottom-right {
    animation-name: Toastify__slideInRight;
  }
  .Toastify__slide-exit--top-right,
  .Toastify__slide-exit--bottom-right {
    animation-name: Toastify__slideOutRight;
  }
`;

export const GlobalStyle = createGlobalStyle`
  html {
    height: 100%;
    background-color: ${ThemeMainBackgroundColor};
  }
  body {
    color: ${ThemeTextColor};
    max-width: ${ThemeBodyWidth};
    background-color: ${ThemeBodyBackgroundColor};
    transition: background-color 1s ease-in-out;
  }
  ::selection {
    background-color: rgba(218, 43, 43, 0.94);
    color: #ffffff;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.42);
  }

  ${NotificationStyles}
`;
