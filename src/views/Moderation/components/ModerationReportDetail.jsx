import dayjs from 'dayjs';
import { transparentize } from 'polished';
import React, { useEffect, useState } from 'react';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { MobileMediaQuery } from '../../../componentsNew/SharedStyles';
import BanModal from '../../../componentsNew/BanModal';
import { Button, TextButton } from '../../../componentsNew/Buttons';
import MiniUserInfo from '../../../componentsNew/MiniUserInfo';
import Modal from '../../../componentsNew/Modals/Modal';
import Post from '../../../componentsNew/Post';
import { getReport, resolveReport } from '../../../services/reports';
import { pushSmartNotification } from '../../../utils/notification';
import {
  ThemeFontSizeHeadline,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeTextColor,
} from '../../../utils/ThemeNew';

const ModerationReportDetail = () => {
  const [report, setReport] = useState(undefined);
  const [closed, setClosed] = useState(false);
  const [dismissModalOpen, setDismissModalOpen] = useState(false);
  const [modalOpen, setModalOpen] = useState(false);
  const location = useLocation();
  const { params } = useRouteMatch();

  useEffect(() => {
    async function fetchReport() {
      const result = await getReport(params.id);
      setReport(result);
    }
    if (location.state && location.state.report) {
      setReport(location.state.report);
    } else {
      fetchReport();
    }
  }, [location, params.id]);

  useEffect(() => {
    if (report) {
      setClosed(report.solvedBy !== null);
    }
  }, [report]);

  const handleReport = async () => {
    try {
      await resolveReport(report.id);
      setClosed(true);
    } catch (error) {
      pushSmartNotification({ error: 'Unable to resolve report.' });
    }
  };

  if (!report) {
    return null;
  }

  const relativeTime = (timestamp) => {
    return dayjs().isBefore(dayjs(timestamp)) ? 'just now' : dayjs().to(dayjs(timestamp));
  };

  return (
    <StyledReportDetail>
      <Modal
        title="Dismiss report?"
        submitText="Dismiss"
        submitFn={() => {
          handleReport();
          setDismissModalOpen(false);
        }}
        cancelFn={() => setDismissModalOpen(false)}
        isOpen={dismissModalOpen}
      />
      <BanModal
        userId={report.post.user.id}
        postId={report.post.id}
        submitFn={() => {
          handleReport();
          setModalOpen(false);
        }}
        cancelFn={() => {
          setModalOpen(false);
        }}
        isOpen={modalOpen}
      />
      <div className="report-header">
        <span className="report-title">
          &ldquo;
          {report.reason}
          &rdquo;
        </span>
        {!closed && (
          <div className="report-actions">
            <TextButton onClick={() => setDismissModalOpen(true)}>Dismiss</TextButton>
            <Button onClick={() => setModalOpen(true)} alert>
              Ban user
            </Button>
          </div>
        )}
      </div>
      <div className="report-info">
        Reported in&nbsp;
        <Link
          className="post-link"
          to={`/thread/${report.post.thread.id}/${report.post.page}#post-${report.post.id}`}
        >
          <span className="report-detail report-thread">{report.post.thread.title}</span>
          &nbsp;in&nbsp;
          <span className="report-detail report-thread">{report.post.thread.subforum.name}</span>
        </Link>
      </div>
      <Post
        hideControls
        ratings={[]}
        canRate={false}
        bans={report.post.bans}
        user={report.post.user}
        threadId={report.post.thread.id || report.post.thread}
        threadInfo={report.post.thread.id && report.post.thread}
        postId={report.post.id}
        postBody={report.post.content}
        postDate={report.post.createdAt}
        postPage={report.post.page}
      />
      <div className="report-activity">
        <div className="report-activity-title">Activity</div>
        <div className="report-event">
          <MiniUserInfo user={report.reportedBy} />
          &nbsp;reported this post&nbsp;
          {relativeTime(report.createdAt)}
        </div>
        {report.post.bans.map((ban) => (
          <div key={ban.id} className="report-event">
            <MiniUserInfo user={ban.bannedBy} />
            &nbsp;banned&nbsp;
            <MiniUserInfo user={ban.user} />
            &nbsp;for&nbsp;
            {dayjs(ban.createdAt).to(dayjs(ban.expiresAt), true)}
            &nbsp;with the reason&nbsp;
            <b>
              &quot;
              {ban.banReason}
              &quot;
            </b>
            &nbsp;
            {relativeTime(ban.createdAt)}
          </div>
        ))}
        {report.solvedBy && (
          <div className="report-event">
            <MiniUserInfo user={report.solvedBy} />
            &nbsp;resolved this report&nbsp;
            {report.updatedAt && relativeTime(report.updatedAt)}
          </div>
        )}
      </div>
    </StyledReportDetail>
  );
};

const StyledReportDetail = styled.div`
  .post-link:hover {
    text-decoration: underline;
  }

  .report-header {
    margin-bottom: 10px;
    display: flex;
    align-items: center;

    ${MobileMediaQuery} {
      flex-direction: column-reverse;
      align-items: flex-start;
    }
  }

  .report-actions {
    margin-left: auto;
    flex-shrink: 0;
    display: flex;

    ${MobileMediaQuery} {
      margin-left: unset;
      margin-bottom: 25px;
      flex-direction: row-reverse;
    }
  }

  .report-title {
    font-weight: bold;
    font-size: ${ThemeFontSizeHeadline};
  }

  .report-info {
    font-size: ${ThemeFontSizeLarge};
    margin-bottom: 30px;
    line-height: normal;
    display: flex;
    align-items: center;
  }

  .report-detail {
    font-weight: 600;
  }

  .report-event {
    font-size: ${ThemeFontSizeMedium};
    display: flex;
    align-items: center;
    color: ${(props) => transparentize(0.2, ThemeTextColor(props))};
    margin-bottom: 10px;
  }

  .report-activity {
    border-top: 1px solid ${(props) => transparentize(0.75, ThemeTextColor(props))};
    margin-top: 40px;
    margin-bottom: 30px;
    padding-top: 30px;
  }

  .report-activity-title {
    font-size: ${ThemeFontSizeLarge};
    font-weight: bold;
    margin-bottom: 20px;
  }
`;
export default ModerationReportDetail;
