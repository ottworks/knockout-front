import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

import {
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeTextColor,
  ThemeFontSizeMedium,
  ThemeFontSizeLarge,
} from '../../../utils/ThemeNew';
import { getLatestMotd, createMotd } from '../../../services/messageOfTheDay';
import { Button } from '../../../componentsNew/Buttons';
import { FieldLabel, FieldLabelSmall, TextField } from '../../../componentsNew/FormControls';
import InputError, { StyledInputError } from '../../../componentsNew/InputError';
import { checkBetweenLengths, checkPresence, checkValidURL, validate } from '../../../utils/forms';

const ModerationMotd = () => {
  const [message, setMessage] = useState('');
  const [buttonLink, setButtonLink] = useState('');
  const [buttonName, setButtonName] = useState('');
  const [initialMotd, setInitialMotd] = useState('');
  const [errors, setErrors] = useState({});

  const checkButtonLinkPresence = (value) => {
    if (buttonName && checkPresence(value)) {
      return 'A button with a name must have a link';
    }
    return undefined;
  };

  const checkButtonNamePresence = (value) => {
    if (buttonLink && checkPresence(value)) {
      return 'A button with a link must have a name';
    }
    return undefined;
  };

  const motdValidators = {
    message: [(value) => checkBetweenLengths(value, 0, 100)],
    buttonLink: [checkButtonLinkPresence, (value) => buttonName && checkValidURL(value)],
    buttonName: [checkButtonNamePresence, (value) => checkBetweenLengths(value, 0, 18)],
  };

  useEffect(() => {
    const getMotd = async () => {
      try {
        const motd = await getLatestMotd();
        setMessage((current) => motd.message || current);
        setButtonLink((current) => motd.buttonLink || current);
        setButtonName((current) => motd.buttonName || current);
        setInitialMotd({
          message: motd.message,
          buttonLink: motd.buttonLink,
          buttonName: motd.buttonName,
        });
      } catch (error) {
        console.error(error);
      }
    };

    getMotd();
  }, []);

  useEffect(() => {
    const results = validate({ message, buttonLink, buttonName }, motdValidators);
    setErrors(results);
  }, [message, buttonLink, buttonName]);

  const submitMotd = async () => {
    if (message === undefined || buttonName === undefined || buttonLink === undefined) {
      return;
    }

    if (
      message.trim() === initialMotd.message &&
      buttonName.trim() === initialMotd.buttonName &&
      buttonLink.trim() === initialMotd.buttonLink
    ) {
      return;
    }

    await createMotd({ message, buttonName, buttonLink });
    window.location.reload();
  };

  return (
    <StyledMotdPage>
      <div className="motd-desc">
        <p>
          The MOTD (Message of the Day) is a message that all site visitors will see on the top of
          the page, under the site header.
        </p>
        <p>
          They will be able to dismiss the MOTD and keep their dismissal remembered in a cookie, but
          any update to the MOTD will make the message reappear for all users.
        </p>
        <p>If the MOTD title is made blank, then no MOTD will be shown.</p>
      </div>
      <div className="motd-form">
        <FieldLabel>Message title</FieldLabel>
        <FieldLabelSmall>Ex. &quot;Check out our new announcement feature!&quot;</FieldLabelSmall>
        <TextField value={message} onChange={(e) => setMessage(e.target.value)} />
        <InputError error={errors.message} />

        <FieldLabel>Message details link (optional)</FieldLabel>
        <FieldLabelSmall>
          A link visitors can follow for more detail about the announcement, e.g. a forum thread or
          Twitch stream
        </FieldLabelSmall>
        <TextField value={buttonLink} onChange={(e) => setButtonLink(e.target.value)} />
        <InputError error={errors.buttonLink} />

        <FieldLabel>Message details button name (optional)</FieldLabel>
        <FieldLabelSmall>Ex. &quot;Read more&quot; or &quot;Watch now&quot;</FieldLabelSmall>
        <TextField value={buttonName} onChange={(e) => setButtonName(e.target.value)} />
        <InputError error={errors.buttonName} />

        <Button
          onClick={submitMotd}
          disabled={errors.message || errors.buttonLink || errors.buttonName}
        >
          Submit
        </Button>
      </div>
    </StyledMotdPage>
  );
};

export default ModerationMotd;

const StyledMotdPage = styled.div`
  .motd-desc {
    font-size: ${ThemeFontSizeLarge};
  }

  .motd-form {
    background: ${ThemeBackgroundDarker};
    padding: calc(${ThemeVerticalPadding} * 2) calc(${ThemeHorizontalPadding} * 2);
    margin-top: 40px;
  }

  ${StyledInputError} {
    margin-top: -20px;
    margin-bottom: 20px;
  }

  textarea.motd-content {
    width: 100%;
    height: 120px;
    border: none;
    border-radius: 0;
    background: ${ThemeBackgroundDarker};
    color: ${ThemeTextColor};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    margin-bottom: ${ThemeVerticalPadding};
    font-size: ${ThemeFontSizeMedium};
    box-sizing: border-box;
    resize: none;

    &:focus {
      outline: 1px solid ${ThemeBackgroundLighter};
    }
  }
`;
