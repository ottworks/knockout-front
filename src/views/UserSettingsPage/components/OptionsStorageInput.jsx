/* eslint-disable jsx-a11y/label-has-for */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { saveValue, loadValue } from '../../../utils/postOptionsStorage';
import { pushSmartNotification } from '../../../utils/notification';
import { FieldLabel, FieldLabelSmall } from '../../../componentsNew/FormControls';
import SettingsToggle from '../../../componentsNew/SettingsToggle';

const OptionsStorageInput = ({ storageKey, defaultValue, desc, label, onChange }) => {
  const [value, setValue] = useState(loadValue(storageKey, defaultValue));

  const updateValue = (newValue) => {
    setValue(newValue);
    saveValue(storageKey, newValue);
    pushSmartNotification({ message: 'Profile preference saved successfully.' });
  };

  return (
    <SettingsToggle
      label={label}
      desc={desc}
      value={value}
      setValue={() => {
        updateValue(!value);
        if (onChange) {
          onChange(!value);
        }
      }}
    />
  );
};

export const StyledOptionsStorageInput = styled.div`
  display: flex;
  justify-content: space-between;
  line-height: normal;
  align-items: center;
  margin-bottom: 20px;
  padding: 0 20px;

  ${FieldLabel} {
    margin-bottom: 5px;
    font-weight: 600;
  }

  ${FieldLabelSmall} {
    margin-bottom: 0px;
  }
`;

OptionsStorageInput.propTypes = {
  label: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  storageKey: PropTypes.string,
  defaultValue: PropTypes.bool,
  onChange: PropTypes.func,
};

OptionsStorageInput.defaultProps = {
  storageKey: '',
  defaultValue: false,
  onChange: null,
};

export default OptionsStorageInput;
