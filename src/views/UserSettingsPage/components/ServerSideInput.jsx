/* eslint-disable jsx-a11y/label-has-for */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import SettingsToggle from '../../../componentsNew/SettingsToggle';

const ServerSideInput = (props) => {
  const [value, setValue] = useState(false);

  useEffect(() => {
    const fetchValue = async () => {
      const serverSideValue = await props.getValue();
      setValue(serverSideValue);
    };
    fetchValue();
  }, []);

  const updateValue = (newValue) => {
    const setValueAsync = async () => {
      await props.setValue(newValue);
      setValue(newValue);
    };
    setValueAsync();
  };
  const { desc, label } = props;

  return (
    <SettingsToggle
      label={label}
      desc={desc}
      value={value}
      setValue={() => {
        updateValue(!value);
      }}
    />
  );
};

ServerSideInput.propTypes = {
  label: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  setValue: PropTypes.func,
  getValue: PropTypes.func,
};

ServerSideInput.defaultProps = {
  setValue: null,
  getValue: null,
};

export default ServerSideInput;
