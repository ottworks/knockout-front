import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import EventCard from './EventCard';

import { ThemeHorizontalPadding, ThemeBackgroundDarker } from '../../../utils/ThemeNew';

const EventsComponent = ({ events, eventsLoaded }) => {
  return (
    <StyledEventsPage>
      <h2>Knockout events from mods and users</h2>
      <ul className="events">
        {eventsLoaded &&
          events.length > 0 &&
          events.map((event, i) => (
            <EventCard
              key={event.id}
              content={event.content}
              createdAt={event.created_at}
              index={i}
            />
          ))}
        {eventsLoaded && !events.length && (
          <li className="empty-list">
            <p>Looks like there are no events </p>
          </li>
        )}
      </ul>
    </StyledEventsPage>
  );
};

EventsComponent.propTypes = {
  events: PropTypes.arrayOf(PropTypes.object).isRequired,
  eventsLoaded: PropTypes.bool.isRequired,
};

export const StyledEventsPage = styled.div`
  padding: 0 ${ThemeHorizontalPadding};
  .events {
    list-style: none;
    padding: 0;
  }
  .empty-list {
    font-size: 2.8rem;
    padding: 30px 0;
    text-align: center;
    text-transform: capitalize;
    opacity: 0.3;
  }
  h2 {
    padding: 20px;
    letter-spacing: 0.05em;
    opacity: 0.8;
    background-color: ${ThemeBackgroundDarker};
    line-height: 20px;
  }
  @media (max-width: 700px) {
    .empty-list {
      font-size: 1.5rem;
    }
    h2 {
      font-size: 0.8em;
    }
  }
`;

export default EventsComponent;
