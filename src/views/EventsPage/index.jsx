import React from 'react';
import { Helmet } from 'react-helmet';
import getEventsList from '../../services/events';

import EventsComponent from './components/EventsComponent';
import { isLoggedIn } from '../../componentsNew/LoggedInOnly';

class EventsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      eventsLoaded: false,
    };
  }

  componentDidMount = async () => {
    const events = await getEventsList();
    this.setState({ events, eventsLoaded: true });
  };

  render() {
    if (!isLoggedIn()) {
      window.location.replace('/login');
      return null;
    }

    const { events, eventsLoaded } = this.state;
    return (
      <>
        <Helmet>
          <title>Event Log - Knockout!</title>
        </Helmet>
        <EventsComponent events={events} eventsLoaded={eventsLoaded} />
      </>
    );
  }
}

export default EventsPage;
