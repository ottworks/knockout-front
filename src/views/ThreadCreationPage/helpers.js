/* eslint-disable import/prefer-default-export */
import { createNewThread } from '../../services/threads';
import { pushSmartNotification } from '../../utils/notification';
import { createAlertRequest } from '../../services/alerts';
import { loadAutoSubscribeFromStorageBoolean } from '../../services/theme';

const getTagId = (str) => str.split('|')[0];

export const submitThread = async (
  title,
  content,
  selectedIconId,
  backgroundUrl,
  backgroundType,
  selectedTags,
  subforumId,
  history
) => {
  try {
    const tagIds = selectedTags.map((tagString) => getTagId(tagString));

    let backgroundOptions = {};
    if (backgroundUrl) {
      backgroundOptions = { backgroundUrl, backgroundType };
    }

    const thread = await createNewThread({
      title,
      icon_id: selectedIconId,
      subforum_id: subforumId,
      content,
      tagIds,
      ...backgroundOptions,
    });

    if (!thread || !thread.id) {
      throw new Error({ error: 'Could not create thread' });
    }

    if (loadAutoSubscribeFromStorageBoolean()) {
      await createAlertRequest({
        threadId: thread.id,
        lastSeen: new Date(),
        previousLastSeen: new Date('01/01/1980'),
        previousLastPostNumber: 1,
        lastPostNumber: 1,
      });
    }

    history.push(`/thread/${thread.id}`);
  } catch (err) {
    if (err.response && err.response.status === 403) {
      pushSmartNotification({ error: 'Your account is too new to post in this subforum.' });
    } else if (err.error) {
      pushSmartNotification({ error: err.error });
    } else {
      pushSmartNotification({ error: 'Thread creation failed' });
    }

    console.error('Error creating thread:', err);
    return false;
  }

  return true;
};
