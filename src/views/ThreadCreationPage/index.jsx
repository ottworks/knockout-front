/* eslint-disable react/forbid-prop-types */
import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';

import { Helmet } from 'react-helmet';
import { getTags } from '../../services/tags';
import { submitThread } from './helpers';
import {
  checkPresence,
  checkBetweenLengths,
  checkUserGroup,
  checkIsImage,
  ifPresent,
  validate,
} from '../../utils/forms';
import { pushSmartNotification } from '../../utils/notification';
import { GOLD_MEMBER_GROUPS } from '../../utils/userGroups';
import { POST_CHARACTER_LIMIT } from '../../utils/limits';

import IconSelector from '../../componentsNew/IconSelector';
import Tooltip from '../../componentsNew/Tooltip';
import UserGroupRestricted from '../../componentsNew/UserGroupRestricted';
import EditorBB from '../../componentsNew/EditorBB';
import InputError from '../../componentsNew/InputError';
import ThreadCreationNotice from './components/ThreadCreationNotice';

import StyledThreadCreation from './style';
import { Tag } from '../../componentsNew/Buttons';
import { isLoggedIn } from '../../componentsNew/LoggedInOnly';

const threadValidators = {
  title: [checkPresence, (value) => checkBetweenLengths(value, 4, 140)],
  content: [checkPresence, (value) => checkBetweenLengths(value, 4, POST_CHARACTER_LIMIT)],
  selectedIconId: [checkPresence],
  backgroundUrl: [
    () => checkUserGroup(GOLD_MEMBER_GROUPS),
    (value) => ifPresent(value, checkPresence),
    (value) => ifPresent(value, checkIsImage),
  ],
};

const ThreadCreationComponent = () => {
  const [errors, setErrors] = useState({});
  const [selectedIconId, setSelectedIconId] = useState(undefined);
  const [title, setTitle] = useState('');
  const [backgroundUrl, setBackgroundUrl] = useState('');
  const [backgroundType, setBackgroundType] = useState('cover');
  const [tags, setTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const [content, setContent] = useState('');
  const [submitting, setSubmitting] = useState(false);

  const { id: subforumId } = useParams();
  const history = useHistory();

  useEffect(() => {
    const grabTags = async () => {
      setTags(await getTags());
    };

    grabTags();
  }, []);

  if (!isLoggedIn()) {
    window.location.replace('/login');
    return null;
  }

  const handleBackgroundTypeChange = (e) => {
    setBackgroundType(e.target.value);
  };

  const handleTagAdd = (e) => {
    const { value } = e.target;

    setSelectedTags([...selectedTags, value]);
    e.target.value = 'default';
  };

  const handleTagRemove = (index) => {
    setSelectedTags(selectedTags.filter((tag, i) => i !== index));
  };

  const getTagName = (str) => str.split('|')[1];

  const handleSubmit = async () => {
    setSubmitting(true);

    const formErrors = validate(
      {
        title,
        content,
        selectedIconId,
        backgroundUrl,
      },
      threadValidators
    );

    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
      pushSmartNotification('Unable to make thread, check the form for errors.');
    } else {
      await submitThread(
        title,
        content,
        selectedIconId,
        backgroundUrl,
        backgroundType,
        selectedTags,
        Number(subforumId),
        history
      );
    }
    setSubmitting(false);
  };

  return (
    <StyledThreadCreation>
      <Helmet>
        <title>{`${title || 'Create New Thread'} - Knockout!`}</title>
      </Helmet>
      <ThreadCreationNotice />
      <IconSelector
        selectedId={selectedIconId}
        handleIconChange={(iconId) => setSelectedIconId(iconId)}
        error={errors.selectedIconId}
      />

      <div className="input">
        <label className="input-label" aria-label="title">
          <span>Title</span>
          <input
            type="text"
            name="title"
            placeholder="Internet Drama"
            onChange={(e) => setTitle(e.target.value)}
            value={title}
          />
        </label>
      </div>
      <InputError error={errors.title} />

      <UserGroupRestricted userGroupIds={GOLD_MEMBER_GROUPS}>
        <div className="line-option-wrapper">
          <div className="input tall">
            <Tooltip fullWidth text="You will get absolutely BTFO if you abuse this!">
              <div className="tooltip-content">
                <label className="input-label" aria-label="background-url">
                  <span>Thread background URL (optional)</span>
                  <input
                    type="text"
                    name="background-url"
                    placeholder="https://onlinewebimages.com/i/epic_image.png"
                    onChange={(e) => setBackgroundUrl(e.target.value)}
                    value={backgroundUrl}
                  />
                </label>
              </div>
            </Tooltip>
          </div>

          <div className="input">
            <label className="input-label" htmlFor="background-type">
              <span>Thread background type</span>
              <div className="background-type-input">
                <div className="background-type-input-inner">
                  <Tooltip text="Cover (fills the whole page in the background)">
                    <label className={backgroundType === 'cover' ? 'active' : ''} htmlFor="cover">
                      <i className="fas fa-expand-arrows-alt" />
                      <input
                        type="radio"
                        value="cover"
                        id="cover"
                        name="background-type"
                        aria-label="Background type - Cover"
                        checked={backgroundType === 'cover'}
                        onChange={handleBackgroundTypeChange}
                        hidden
                      />
                    </label>
                  </Tooltip>
                  <Tooltip text="Tiled (best for small images, gets repeated in the background)">
                    <label className={backgroundType === 'tiled' ? 'active' : ''} htmlFor="tiled">
                      <i className="fas fa-puzzle-piece" />
                      <input
                        type="radio"
                        value="tiled"
                        id="tiled"
                        name="background-type"
                        aria-label="Background type - Tiled"
                        checked={backgroundType === 'tiled'}
                        onChange={handleBackgroundTypeChange}
                        hidden
                      />
                    </label>
                  </Tooltip>
                </div>
              </div>
            </label>
          </div>
        </div>
        <InputError error={errors.backgroundUrl} />
      </UserGroupRestricted>

      {tags.length > 0 && (
        <div className="line-option-wrapper">
          <div className="input tall tags-list">
            <label className="input-label" htmlFor="tags">
              <span>Tags</span>

              <select id="tags" onChange={handleTagAdd}>
                <option value="default">Select a tag...</option>
                {tags.map((tag) => (
                  <option
                    key={`tag_${tag.id}`}
                    value={`${tag.id}|${tag.name}`}
                    disabled={selectedTags.includes(`${tag.id}|${tag.name}`)}
                  >
                    {tag.name}
                  </option>
                ))}
              </select>
            </label>
          </div>
          <div className="input">
            <div className="input-label">
              <span>Selected Tags</span>
              <div className="selected-tags">
                {selectedTags.map((tag, index) => {
                  return (
                    <Tag key={tag} type="button" onClick={() => handleTagRemove(index)}>
                      <i className="fas fa-times-circle" />
                      &nbsp;
                      {getTagName(tag)}
                    </Tag>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="editor">
        <EditorBB content={content} setContent={(value) => setContent(value)}>
          <button type="submit" disabled={submitting} onClick={handleSubmit}>
            <i className="fas fa-paper-plane" />
            Submit
          </button>
        </EditorBB>
        <InputError error={errors.content} />
      </div>
    </StyledThreadCreation>
  );
};

export default ThreadCreationComponent;
