import { loadProgressBar } from 'axios-progress-bar';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import Modal from 'react-modal';
import MainView from './views/MainView';
import store from './state/configureStore';
import AppThemeProvider from './AppThemeProvider';

// adds the axios progress bar to the global axios instance
loadProgressBar({
  parent: '#knockout-header',
});
toast.configure();

Modal.setAppElement('#app');

const App = () => {
  return (
    <Provider store={store}>
      <AppThemeProvider>
        <BrowserRouter basename="/">
          <MainView />
        </BrowserRouter>
      </AppThemeProvider>
    </Provider>
  );
};

export default App;

ReactDOM.render(<App />, document.getElementById('app'));
