#!/usr/bin/env node
const fs = require('fs');
const icons = require('./icons.json');

let id = 29;

icons.icons.forEach(icon => Object.assign(icon, { id: ++id }));
fs.writeFileSync('icons.json', JSON.stringify(icons, null, 2));
